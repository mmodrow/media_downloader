# README #

### What does MediaDownloader do? ###

* I am an avid web comic reader and love web animations. The normal process of downloading comics for later reference (or to be prepared for when the website goes down for good) for me was to go to a comic page, decide if there was an HD-version of the comic, right click, save image/link as, look for the right folder, decide how to name it (extra fun for comics like the phd comic where default naming is phdMMDDYY.gif - perfect for sorting by name, you know?) and then download it.

    Most of those actions can easily be automated!

    This is why I created MediaDownloader. There you have an XML-file describing what domains you'd like to download, how they work, where to save the images from there and how to name them. Even how to identify HD versions if available.

    All you need to do once the xml has all you need is to put xml and jar together somewhere, start the jar and start dragging & dropping urls directly from the browser address bar or anchor tags into the console window of the MediaDownloader GUI.

    It supports both relative and absolute paths, so you could put one xml file with relative paths on your thumb drive and another with absolute paths spread all over you desktop PC on the machine at home.

    It has also support for multi-page-comics, as long as they are all within the same page and can be targeted with a common CSS selector (e.g. you have "http://www.comicdomain.com/index.php?comic=1234" and there all 4 images relating to the comic can be found via "#comic img"). In this case the files will be saves as "imageFileName_1", "imageFileName_2", "imageFileName_3" & "imageFileName_4".

* Version 1.0.0
    * does all the basic needs:
        * A GUI with console output
        * drag'n'drop from the browser
        * configuration of download setup via xml file
        * multi file comics from a single page
        * copying text from the console window
        * multi-threading to keep the GUI responsive while parsing & downloading
        * both linux and windows directory separators
        * fixing naming collisions on download file names (extra common on thecodinglove.com) by adding underscore+unix timestamp to the file name.
* Known issues
    * it is still quite slow
    * the architecture is still quite wacky
        * most likely the Console & MediaDownloaderGui classes could be merged
    * the GUI is still quite ugly
    * the build process - at least for me - still is a pain in the ass to set up
    * I should start writing some tests, I guess...
    * the error handling e.g. on bad http requests could definitely use some improvement
    * I have not yet thought about how to get different comics from the same domain downloading into different folders...

### How do I get set up? ###

* There is no installation needed and no external data saving (other than the config file and the intended downloads). MediaDownloader works fully portable on both Windows (tested only on W10, but I didn't use any crazy stuff, as far as I know so it should be fine at least down to W7) and Linux (tested the download class on Ubuntu 14.04.4 LTS via console, no GUI tests on Linux so far)
* Configuration
    * If you are happy with my configured sites, you just need to open the domains.xml and change the paths as needed. If you wish to add new domains, you can extend the domains.xml accordingly.
    * There is a [documentation](https://bitbucket.org/mmodrow/media_downloader/wiki/domains.xml) on how to work with the domains.xml
* Dependencies
    * The only external dependency I have is jsoup 1.9.2 for requesting the html via web & parsing the response body dom. It's included in the repo.
        * JSOUP is available under the MIT license - https://jsoup.org/license
    * The Media Downloader needs to be compiled and run with at least Java Java 8u101 (tested with Java 8u131) in order to support letsencrypt-encrypted https sites.
* Compilation Instructions
    * n/a, yet. Got it in my IntelliJ, but no clear idea how to put it. Basically shove everything into a jar, include jsoup into the mix and have MediaDownloaderGui as the main class...
    
### LICENSE ###
* This software is licensed under the GNU Affero General Public License v3.0 (http://www.gnu.org/licenses/agpl-3.0.txt)
    * Feel free to
        * use this commercially or privately
        * distribute it
        * modify it
    * as long as you
        * disclose your source as well
        * use the same license and share it alongside your code
        * make clear, what you changed if you did change the program
    * I am not to be held liable for anything that might go wrong. I surely didn't intend any malicious behaviour, but as this is a hobby project I cannot guarantee anything.
    * if you choose to modify and/or distribute this, please attribute this repository as its origin and I'd really appreciate a little note about it.

### Contribution guidelines ###

* Writing tests
    * n/a yet
* Code review
    * n/a yet
* Other guidelines
    * n/a yet

### Who do I talk to? ###

* The Owner of this repo is [mmodrow](https://bitbucket.org/mmodrow/) Feel free to send messages of pull requests if you have something on your mind.
* No other contributors, yet.