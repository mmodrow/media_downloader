package UI;

import Parsing.DownloadLogic;
import Text.Logger;

import javax.swing.*;
import java.awt.*;
import java.awt.datatransfer.*;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.PrintStream;

/**
 * Created by mmodrow on 20.08.2016.
 * Handles the textArea in the GUI that is the dropTarget
 */
public class DropTarget implements Runnable  {
  
  // TODO: Refactor into meaningful chunks.
  @SuppressWarnings("WeakerAccess")
  protected static final DropTarget ourInstance = new DropTarget();
  public static boolean quit;
  
  /**
   * The Text area.
   */
  @SuppressWarnings("WeakerAccess")
  protected final JTextArea textArea  = new JTextArea();
  private   final Thread    reader1   = new Thread(this);
  private   final Thread    reader2   = new Thread(this);
  
  private final PipedInputStream pin1 = new PipedInputStream();
  private final PipedInputStream pin2 = new PipedInputStream();
  
  /**
   * The Error thrower.
   * Just for testing (Throws an Exception at this MediaDownloader.
   */
  @SuppressWarnings("WeakerAccess")
  protected final Thread errorThrower = new Thread(this);
  
  /**
   * Constructor
   */
  private DropTarget (){
    initialiseTextArea();
    initialiseThreads();
    initialiseStreams();
  }
  
  /**
   * Starting two separate threads to read from the PipedInputStreams
   */
  @SuppressWarnings("WeakerAccess")
  protected void initialiseThreads(){
    //
    reader1.setDaemon(true);
    reader1.setName("Reader 1");
    reader1.start();
    //
    reader2.setDaemon(true);
    reader2.setName("Reader 2");
    reader2.start();
    
    errorThrower.setDaemon(true);
    errorThrower.setName("Error Thrower");
    errorThrower.start();
  
  }
  
  /**
   * prepare the streams piping system.out to the textArea
   */
 @SuppressWarnings("WeakerAccess")
 protected void initialiseStreams() {
    try {
      PipedOutputStream pout = new PipedOutputStream(this.pin1);
      System.setOut(new PrintStream(pout, true));
    } catch (IOException | SecurityException io) {
      DropTarget.getTextArea().append("Couldn't redirect STDOUT to this console\n" + io.getMessage());
    }
  
    try {
      PipedOutputStream pout2 = new PipedOutputStream(this.pin2);
      System.setErr(new PrintStream(pout2, true));
    } catch (IOException | SecurityException io) {
      DropTarget.getTextArea().append("Couldn't redirect STDERR to this console\n" + io.getMessage());
    }
 }
  /**
   * get the text area that resembles the actual area to drop stuff into up to speed
   */
 @SuppressWarnings("WeakerAccess")
 protected void initialiseTextArea() {
    textArea.setEditable(false);
    textArea.append("Drag'n'Drop urls from the browser here to download their media if configured correctly.\n");
  
    String keyStrokeAndKey = "copy";
    KeyStroke keyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_C, InputEvent.CTRL_MASK);
  
    Action action = new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent e) {
        copySelection();
      }};
    textArea.getInputMap().put(keyStroke, keyStrokeAndKey);
    textArea.getActionMap().put(keyStrokeAndKey, action);
    
    setUpTextAreaTransferHandler();
 }
  
 /**
  * TODO: Comment.
  */
  @SuppressWarnings("WeakerAccess")
  protected void setUpTextAreaTransferHandler() {
    
    textArea.setTransferHandler(new TransferHandler() {
      @Override
      public boolean canImport(final TransferSupport support) {
        return true;
      }
    
      @Override
      public boolean importData(final TransferSupport support) {
        final Transferable transferable = support.getTransferable();
        try {
          String targetUrl = (String) transferable.getTransferData(DataFlavor.stringFlavor);
          String[] urlsByLine = targetUrl.split("\r\n|\r|\n");
          new Thread(() -> processDroppedUrl(urlsByLine)).start();
        } catch (UnsupportedFlavorException | IOException e) {
          e.printStackTrace();
          return false;
        }
        
        return true;
      }
    });
  }
  
  /**
   * TODO: Comment
   * TODO: Split
   */
  public synchronized void run() {
    try {
      while (Thread.currentThread() == reader1) {
        try {
          this.wait(100);
        } catch (InterruptedException ie) {/*  */}
        
        if (pin1.available() != 0) {
          String input = this.readLine(pin1);
          textArea.append(input);
          textArea.setCaretPosition(textArea.getDocument().getLength());
        }
        
        if (quit) return;
      }
      
      while (Thread.currentThread() == reader2) {
        try {
          this.wait(100);
        } catch (InterruptedException ie) {/*  */}
        
        if (pin2.available() != 0) {
          String input = this.readLine(pin2);
          textArea.append(input);
          textArea.setCaretPosition(textArea.getDocument().getLength());
        }
        
        if (quit) return;
      }
    } catch (Exception e) {
      textArea.append("\nConsole reports an Internal error.");
      textArea.append("The error is: " + e);
    }
  }
  
  /**
   * get an instance of this singleton.
   */
  @SuppressWarnings("SameReturnValue")
  public static DropTarget getInstance () {
    return ourInstance;
  }
  
  /*
   * get the text area object for the drop target area
   */
  public static JTextArea getTextArea () {
    return ourInstance.textArea;
  }
  
  /**
   * perform the download and output the result.
   * @param targetUrls what page should be analysed for downloading?
   * @return where images downloaded successfully?
   */
  @SuppressWarnings("WeakerAccess")
  public synchronized static boolean processDroppedUrl(String[] targetUrls) {
    boolean errorOccurred = false;
    for(String targetUrl : targetUrls){
      if(!processDroppedUrl(targetUrl)){
        errorOccurred = true;
      }
    }
    return errorOccurred;
  }
  
  @SuppressWarnings("WeakerAccess")
  public synchronized static boolean processDroppedUrl(String targetUrl) {
    String[] downloadedTo = DownloadLogic.processUrl(targetUrl);
    if (null != downloadedTo && downloadedTo.length > 0) {
      Logger.log("downloaded to " + String.join(", ", downloadedTo), true, true);
    } else {
      Logger.log("nothing was downloaded", true, true);
    }
    
    return null != downloadedTo && downloadedTo.length > 0;
  }
  
  /**
   * copies the selected text from the dropTarget textArea to the system clipboard
   */
  public void copySelection(){
    StringSelection stringSelection = new StringSelection(textArea.getSelectedText());
    Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
    clipboard.setContents(stringSelection, null);
  }
  
  public void closeReaders(){
    try {
      reader1.join(1000);
      pin1.close();
    } catch (Exception e) {/*  */}
    
    try {
      reader2.join(1000);
      pin2.close();
    } catch (Exception e) {/*  */}
  }
  
  /**
   * Read line string.
   *
   * @param pipedInputStream the in
   * @return the string
   * @throws IOException the io exception
   */
  @SuppressWarnings("WeakerAccess")
  protected synchronized String readLine(PipedInputStream pipedInputStream) throws IOException {
    StringBuilder input = new StringBuilder();
    do {
      int available = pipedInputStream.available();
      if (available == 0) break;
      byte b[] = new byte[available];
      //noinspection ResultOfMethodCallIgnored
      pipedInputStream.read(b);
      input.append(new String(b, 0, b.length));
    } while (!input.toString().endsWith("\n") && !input.toString().endsWith("\r\n") && !quit);
    
    return input.toString();
  }
}