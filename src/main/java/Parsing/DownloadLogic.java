package Parsing;

import Config.Domain;
import Config.Image;
import Config.Nsfw;
import IO.FileSystem;
import IO.HttpRequest;
import IO.HttpRequestException;
import Notification.FlashBack;
import Text.Logger;
import Text.StringUtils;
import UI.DropTarget;

import java.io.File;
import java.net.URL;

/**
 * The MediaDownloader is a standalone snippet to drop some url of a know. website onto to download some media data based on a default schema.
 * It has its own main function and needs only an url as parameter.
 */
public abstract class DownloadLogic {
  // TODO: Refactor into meaningful chunks.
  public static final boolean DOWNLOAD = true;
  
  /**
   *. TODO: Comment
   */
  public static String[] processUrl (String pageUrl) {

    /* TODO: 9gag Fix
       9Gag switched to JSON-powered markup building.
       Regex to get the JSON from the markup: Match group 1 of »GAG\.App\.loadConfigs\((.*?)\)\.loadAsynScripts\(«
       within BODY
       image infos lie in
       JSON.data.post
                     .title [headline]
                     .type [Animated|Photo]
                     .nsfw [0|1]
                     .images.
                             image700
                             image460
                             image460sv [MP4 VIDEO]
                             image460svm [WEBM VIDEO]
     */

    long before = System.currentTimeMillis();
    String[] parsed = null;
    Logger.log("---------------------------------------\nparsing: " + pageUrl + "\n---------------------------------------", true, true);
    try {
      parsed = parseUrl(pageUrl);
      if (null != parsed && parsed.length > 0) {
        Logger.log("Everything is good!", true, true);
      } else {
        Logger.log("The given url seems unfamiliar.", true, true);
      }
    } catch (Exception e) {
      Logger.log("Domain String couldn't be parsed. An exception occurred.", true, true);
      e.printStackTrace();
    }
    Logger.log("\n\n\n");
    long after = System.currentTimeMillis();
    Logger.log("Elapsed Time: "+(after-before)+"ms", true, true);
    return parsed;
  }
  
  
  /**
   * checks if the given url refers to a parsable page on a known domain
   * TODO: Split
   *
   * @param urlString url to be parsed
   * @return success-state
   */
  private static String[] parseUrl(String urlString) throws Exception {
    if (urlString.length() > 1) {
      URL url;
      try {
        url = new URL(urlString);
      } catch(java.net.MalformedURLException exception){
        Logger.log("Given Url was not valid.", true, true);
        FlashBack.errorFlash(DropTarget.getTextArea());
        return new String[0];
      }
      
      String domain = url.getHost().toLowerCase();
      String path = url.getPath();
      String query = url.getQuery();
      path += (query != null) ? ("?" + query) : "";
      String domainName = Domain.fromName(domain);
      String[][] parsedUrls;
      String[] downloaded = null;
      boolean downloadedSomething = false;
      if (null != domainName && domainName.length() > 0) {
        
        path = Domain.pathPatternReplace(domainName, path);
        
        if (null != query && query.length() > 0 && Domain.useQuery(domainName)){
          path = path+query;
        }
        
        Logger.log("domain: " + domainName + "\npath: " + path);
        parsedUrls = parseFromDomainNode(path, domainName);
        int numUrls = parsedUrls.length;
        if (numUrls > 0 && null == parsedUrls[0][0]){
          numUrls = 0;
        }
        
        downloaded = new String[numUrls];
        for (int i = 0; i < numUrls; i++) {
          if (null != parsedUrls[i][0] && null != parsedUrls[i][1]) {
            downloaded[i] = HttpRequest.download(parsedUrls[i][0], parsedUrls[i][1]);
            if (null != downloaded[i] && downloaded[i].length() > 0) {
              downloadedSomething = true;
            }
          }
        }
        
        Logger.log("name of suited domain: " + domainName);
      }
      if (downloadedSomething) {
        Logger.log("Download successful.", true, true);
        return downloaded;
      }
      
      Logger.log("Domain not in use, yet or nothing was found there.", true, true);
    }
    
    Logger.log("given url too short");
    FlashBack.errorFlash(DropTarget.getTextArea());
    return new String[0];
  }

  /**
   * parse actual data from given path and configuration domain node & name
   * TODO: Split
   *
   * @param path       what came behind the domain in the requested url
   * @param domainName the domain node name of the domain that corresponds to the given url
   * @return success state
   */
  private static String[][] parseFromDomainNode (String path, String domainName) throws HttpRequestException {
    // if some http problem occurs that shouldn't break everything
    String pathPattern = Domain.pathPattern(domainName);
    if (path.matches(pathPattern)) {

      String domain = StringUtils.urlPrependProtocol(Domain.url(domainName));
      
      String requestUrl = domain + path;
      validateReachable(requestUrl);
      loginIfNecessary(domainName, requestUrl);
      
      String folder;
      try {
        folder = Image.folder(domainName);
        folder = folder.replace('/', File.separator.charAt(0));
      } catch (Exception e){
        folder = "./"+domainName;
      }
  
      folder += appendNsfwFolderIfNecessary(domainName, requestUrl);
      
      Logger.log("\ndomain URL: " + domain+"\nfolder: " + folder+"\npattern: " + pathPattern);

      DownloadUrlCollection urlCollection = new DownloadUrlCollection(domainName, requestUrl);
  
      String fileName;
      
      boolean parseJSON = domainName.equals("9gag.com");
      if(parseJSON){
        //TODO: Dynamize!
        String markup = HttpRequest.sendGet(requestUrl);
        String JSONLeadIn = "GAG.App.loadConfigs(";
        int JSONStart = markup.indexOf(JSONLeadIn) + JSONLeadIn.length();
        markup = markup.substring(JSONStart);
        String JSONFinish = ").loadAsynScripts(";
        int JSONEnd = markup.indexOf(JSONFinish);
        markup = markup.substring(0, JSONEnd);
        String regex = ".*?\"post\":.*?\"id\":\"([^\"]*).*?\"title\":\"([^\"]*)\".*?\"nsfw\":(\\d).*?\"images\":.*?\"image700\":.*?\"url\":\"([^\"]*)\".*?\"image460sv\":.*?\"url\":\"([^\"]*).*";
        String replacement = "$1;$2;$3;$4;$5";
        String parsed = markup.replaceAll(regex, replacement);
        String[] parsedSplit = parsed.split(";");
        String id = parsedSplit[0];
        String title = parsedSplit[1];
        boolean nsfw = parsedSplit[2].equals("1");
        String imageUrl = parsedSplit[3].replaceAll("\\\\/", "/");
        if(!imageUrl.contains(id)){
          imageUrl = null;
        }
        String videoUrl = parsedSplit[4].replaceAll("\\\\/", "/");
        if(!videoUrl.contains(id)){
          videoUrl = null;
        }
        Logger.log("fileName: " + title);
        Logger.log("nsfw: " + nsfw);
        Logger.log("imageUrl: " + imageUrl);
        Logger.log("videoUrl: " + videoUrl);
        fileName = StringUtils.sanitiseFileName(title);
      }
      else {
        // getting the name for the file as it should be in the local filesystem
        fileName = DocumentParser.getImageDownloadBaseName(path, domainName);
      }
      // base name stands, now we need the source image for the correct download link and the file ending.
      Logger.log("fileName: " + fileName);
  
      if (DownloadUrlCollection.isEmpty(urlCollection.getRelevantDownloadUrls())){
        return new String[0][0];
      }
      fileName = FileSystem.limitFileNameLength(fileName, ("_"+urlCollection.getRelevantDownloadUrls().length).length());

      String pageDlPath = folder + File.separator;
      String pageDlPathAbsolute = pageDlPath;
      try {
        pageDlPathAbsolute = (new File(pageDlPath)).getCanonicalPath();
      } catch (Exception e) {/* */}
      
      //TODO: analyse possible null pointer Exception
      Logger.log("Initialising download of \n»" + String.join(", ", urlCollection.getRelevantDownloadUrls()) + "« \nto »" + pageDlPathAbsolute + "« \nas »" + fileName + "«.\n", true, true);
      if (DOWNLOAD) {
        String[][] downloadedUrls = new String[urlCollection.getRelevantDownloadUrls().length][2];
        for (int i = 0; i < urlCollection.getRelevantDownloadUrls().length; i++) {
          String thisFileName = fileName;
          if (urlCollection.getRelevantDownloadUrls().length > 1) {
            thisFileName += "_" + (i + 1);
          }
          
          thisFileName += urlCollection.getFileEnding();
          
          String downloadUrl = urlCollection.getUrlById(i);
          if(null != downloadUrl) {
            downloadedUrls[i][0] = downloadUrl;
            downloadedUrls[i][1] = pageDlPath + thisFileName;
          }
        }
        
        return downloadedUrls;
      }
    }
    
    return new String[0][0];
  }
  
  /**
   * Returns the NSFW-directory if it needs to be appended to the download path.
   * @param domainName The domain name.
   * @param requestUrl The request URL.
   * @return The NSFW-directory - or an empty string if it's not needed.
   */
  private static String appendNsfwFolderIfNecessary(String domainName, String requestUrl) {
    boolean isNSFW = DocumentParser.isNSFW(domainName, requestUrl);
    if (isNSFW){
      return File.separator + Nsfw.directory(domainName);
    }
    
    return "";
  }
  
  /**
   * Performs a login if it is necessary.
   * @param domainName The domain name.
   * @param requestUrl The request URL.
   * @throws HttpRequestException Login failed.
   */
  private static void loginIfNecessary(String domainName, String requestUrl) throws HttpRequestException {
    if(DocumentParser.isLoginNecessary(domainName, requestUrl)){
      boolean loginSuccessful = HttpRequest.loginToDomain(domainName);
      if(!loginSuccessful){
        // failed login means no more trying needed
        throw new HttpRequestException("Login failed for: " + requestUrl);
      }
      
      DocumentParser.purgeCaches();
    }
  }
  
  /**
   * Makes sure, that the requested URL is reachable and throws a custom Exception if it is not.
   * @param requestUrl The request URL.
   * @throws HttpRequestException Requested URL was not reachable
   */
  private static void validateReachable(String requestUrl) throws HttpRequestException {
    if(!HttpRequest.isUrlReachable(requestUrl)){
      Logger.log("Url is not reachable.", true, true);
      FlashBack.errorFlash(DropTarget.getTextArea());
      throw new HttpRequestException("Requested URL was not reachable: " + requestUrl);
    }
  }
}
