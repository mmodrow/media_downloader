package Notification;

import java.awt.*;
import java.util.Timer;

/**
 * A task designed to flash to white and then trigger a flash back to the given certain colour when flashes are left
 */
public class FlashBack extends Flash {
  
  /**
   * Constructor
   *
   * @param target     The Component to flash
   * @param timeout    timeout in ms
   * @param numFlashes amount of flashes to c and back to white
   */
  public FlashBack (Component target, Color c, long timeout, int numFlashes) {
    super(target, c, timeout, numFlashes);
  }
  
  /**
   * Flash to a certain color and back a couple of times.
   * @param target     the target component to flash
   * @param timeout    timeout in ms
   * @param numFlashes amount of flashes to c and back to white
   */
  @SuppressWarnings("WeakerAccess")
  public static synchronized void flash (Component target, Color c, @SuppressWarnings("SameParameterValue") long timeout, @SuppressWarnings("SameParameterValue") int numFlashes) {
    Timer timer = new Timer();
    timer.schedule(new FlashBack(target, c, timeout, numFlashes), 0);
  }
  
  /**
   * Flash the drop target background color to signal a problem.
   * @param target The container to flash up in red.
   */
  public static void errorFlash (Component target) {
    flash(target, new Color(255, 0, 0, 255), 100, 5);
  }
  
  /**
   * Flash the drop target background color to signal a problem.
   */
  public static void errorFlash (){
    if (null != defaultTarget) {
      flash(defaultTarget, new Color(255, 0, 0, 255), 100, 5);
    }
  }
  
  /**
   * do the flashing
   */
  @Override public void run()
  {
    target.setBackground(Color.WHITE);
    // switch back to the given color
    Timer timer = new Timer();
    timer.schedule( new Flash(target, c, timeout, numFlashes-1), timeout);
  }
}
