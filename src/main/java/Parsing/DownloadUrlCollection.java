package Parsing;

import IO.HttpRequest;
import IO.HttpRequestException;

/**
 * Created by mmodrow on 17.07.2017.
 * Models reading Urls from a Domain Name and a Request URL to get the most relevant info from them.
 */
public class DownloadUrlCollection {
  
  protected String domainName;
  protected String requestUrl;
  protected String[] imageSdUrls;
  protected String[] imageHdUrls;
  protected String[] relevantDownloadUrls;
  protected String fileEnding;
  
  /**
   * Determines whether an array is empty.
   * @param array
   * @return Whether the array is null or empty.
   */
  public static boolean isEmpty(Object[] array) {
    return !(null != array && array.length > 0);
  }
  
  /**
   * Gets the relevantDownloadUrls.
   * @return relevantDownloadUrls
   */
  public String[] getRelevantDownloadUrls() {
    return relevantDownloadUrls;
  }
  
  /**
   * Get a single download URL from the appropriate array of URLs
   * @param i The index of the URL.
   * @return The URL.
   */
  public String getUrlById(int i){
    if(!isEmpty(relevantDownloadUrls) && relevantDownloadUrls.length >= i){
      if(HttpRequest.isUrlReachable(relevantDownloadUrls[i])){
        return relevantDownloadUrls[i];
      }
    }
    
    if(relevantDownloadUrls != imageSdUrls && !isEmpty(imageSdUrls)) {
      if(HttpRequest.isUrlReachable(imageSdUrls[i])){
        return imageSdUrls[i];
      }
    }
    
    return null;
  }
  
  /**
   * Get the appropriate file Ending for downloads.
   * @return The file ending for the downloads.
   */
  public String getFileEnding() {
    return fileEnding;
  }
  
  /**
   * Constructor
   * @param domainName The name of the domain to handle.
   * @param requestUrl The exact URL of the desired request.
   * @throws HttpRequestException
   */
  public DownloadUrlCollection(String domainName, String requestUrl) throws HttpRequestException {
    this.domainName = domainName;
    this.requestUrl = requestUrl;
    getImageDownloadUrlArrays();
    relevantDownloadUrls = getRelevantDownloadUrls(imageHdUrls, imageSdUrls);
    fileEnding = getFileEndingFromUrls(imageHdUrls, imageSdUrls);
  }
  
  /**
   * Get the arrays with URLs for HD and SD downloads.
   * Saves them to the attributes.
   * @throws HttpRequestException Login failed.
   */
  protected void getImageDownloadUrlArrays() throws HttpRequestException {
    // log in when needed and then try to get download urls
    // TODO: find out why it needs to log in twice...
    for (int i = 0; i < 2 && isEmpty(imageHdUrls) && isEmpty(imageSdUrls); i++){
      // If a Login is necessary log in and try to get the image urls afterwards.
      if(isEmpty(imageHdUrls) && isEmpty(imageSdUrls) && DocumentParser.isLoginNecessary(domainName, requestUrl)){
        boolean loginSuccessful = HttpRequest.loginToDomain(domainName);
        if(!loginSuccessful){
          // failed login means no more trying needed
          throw new HttpRequestException("Login failed.");
        }
        
        DocumentParser.purgeCaches();
      }
      
      // Get download link for low and high resolution image version.
      imageSdUrls = DocumentParser.getImageDownloadUrls(domainName, requestUrl, "sd");
      imageHdUrls = DocumentParser.getImageDownloadUrls(domainName, requestUrl, "hd");
    }
  }
  
  /**
   * Tries to get the appropriate file ending for the URL arrays.
   * @param imageHdUrls HD URLs
   * @param imageSdUrls SD URLs
   * @return The appropriate file ending.
   */
  private String getFileEndingFromUrls(String[] imageHdUrls, String[] imageSdUrls) {
    String fileEnding = getFileEndingFromFirstUrl(imageHdUrls);
    fileEnding = "".equals(fileEnding)? getFileEndingFromFirstUrl(imageSdUrls) : fileEnding;
    
    if("".equals(fileEnding)){
      fileEnding = ".jpg";//TODO: Default file ending per site
    }
    
    return fileEnding;
  }
  
  /**
   * Get the file ending for the first URL from an array.
   * @param imageUrls The array url.
   * @return The file ending.
   */
  private String getFileEndingFromFirstUrl(String[] imageUrls) {
    String[] imgFileEndingSplit;
    String fileEnding = "";
    if(!isEmpty(imageUrls)) {
      imgFileEndingSplit = imageUrls[0].split("/");
      imgFileEndingSplit = imgFileEndingSplit[imgFileEndingSplit.length - 1].split("\\.");
      if(imgFileEndingSplit.length > 1) {
        fileEnding = "."+imgFileEndingSplit[imgFileEndingSplit.length - 1];
      }
    }
    
    return fileEnding;
  }
  
  /**
   * Get the appropriate URL set.
   * @param imageHdUrls The HD URLs.
   * @param imageSdUrls The SD URLs.
   * @return The applicable set of URLs.
   */
  private String[] getRelevantDownloadUrls(String[] imageHdUrls, String[] imageSdUrls) {
    if (!isEmpty(imageHdUrls) && imageHdUrls[0].length() > 0) {
      return imageHdUrls;
    }
    
    if (!isEmpty(imageSdUrls) && imageSdUrls[0].length() > 0) {
      return imageSdUrls;
    }
    
    return null;
  }
}