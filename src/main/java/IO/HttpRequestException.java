package IO;

/**
 * Created by mmodrow on 17.07.2017.
 */
public class HttpRequestException extends Exception{
  public HttpRequestException(String message) {
    super(message);
  }
}
