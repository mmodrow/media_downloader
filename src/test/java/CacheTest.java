import Caching.Cache;
import Caching.CacheAction;
import Config.ReadConfig;
import org.junit.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.*;

public class CacheTest extends Cache<String>
{
  private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
  private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
  
  @Before
  public void setUpStreams() {
    System.setOut(new PrintStream(outContent));
    System.setErr(new PrintStream(errContent));
  }
  
  @Before
  public void setUpDocuments() {
    //
  }
  
  @After
  public void cleanUpStreams() {
    System.setOut(null);
    System.setErr(null);
  }
  
  @After
  public void cleanUpDocuments() {
    
  }
  
  @BeforeClass
  public static void oneTimeSetUp() {
    // one-time initialization code
  }
  
  @AfterClass
  public static void oneTimeTearDown() {
    TestUtils.deleteTestData();
  }
  
  @Before
  public void cleanupFiles(){
    TestUtils.deleteTestData();
  }
  
  
  @Before
  public void setUp() {
    //setup for each individual test
  }
  
  @After
  public void tearDown() {
    //de-setup for each individual test
  }
  
  /**
   * create selector
   */
  @Test
  public void getSelectorTest(){
    String[] selectorParts = {"testA", "testB"};
    assertEquals("testA"+ Cache.cacheDivisor+"testB", getSelector(selectorParts));
  }
  
  @Test
  public void getSelectorNullElementTest(){
    String[] selectorParts = {null, "testB", null};
    assertEquals("null"+ Cache.cacheDivisor+"testB"+ Cache.cacheDivisor+"null", getSelector(selectorParts));
  }
  
  @Test
  public void getSelectorEmptyListTest(){
    String[] selectorParts = {};
    assertEquals("", getSelector(selectorParts));
  }
  
  @Test
  public void getSelectorNullListTest(){
    String[] selectorParts = null;
    //noinspection ConstantConditions
    assertEquals("", getSelector(selectorParts));
  }
  
  @Test
  public void getNoHitTest(){
    assertNull(get("test"));
  }
  
  @Test
  public void getEmptySelectorTest(){
    assertNull(get(""));
  }
  
  @Test
  public void setEmptySelectorTest(){
    assertFalse(set("", "test"));
  }
  
  @Test
  public void setNullValueTest(){
    assertFalse(set("test", null));
  }
  
  @Test
  public void setStringTest(){
    assertTrue(set("testSelector", "testValue"));
  }
  
  
  @Test
  public void setAndGetStringTest(){
    assertTrue(set("testSelector", "testValue"));
    assertEquals("testValue", get("testSelector"));
  }
  
  
  @Test
  public void setAndGetStringSelectorListTest(){
    String[] selectorParts = {"testA", "testB"};
    assertTrue(set(selectorParts, "testValue"));
    assertEquals("testValue", get(selectorParts));
  }
  
  @Test
  public void getWithCallable() {
    String i1 = "tollerString";
    String i2 = "andererTollerString";
    class OneShotTask extends CacheAction<String> {
        private String str1;
        private String str2;
        private OneShotTask (String s1, String s2) { str1 = s1; str2 = s2; }
        public String action() throws Exception {
            return str1+str2;
        }
    }
    CacheAction<String> r1 = new OneShotTask(i1, i2);
    // Callable<String> is just fine here
    @SuppressWarnings(value="unchecked") 
    String output = get("selector", r1);
    assertEquals("tollerStringandererTollerString", output);
  }
}