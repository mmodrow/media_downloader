package Parsing;

import Caching.Cache;
import Caching.CacheAction;
import Config.*;
import Text.Logger;
import Text.Replace;
import Text.StringUtils;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.IOException;
import java.net.URLEncoder;

/**
 * Created by mmodrow on 18.08.2016.
 *
 *
 * Takes care of any directly DOM-related actions
 */
public abstract class DocumentParser {
  
  // TODO: Refactor into meaningful chunks.
  
  private static final Cache<Element> calledDomElement = new Cache <>();
  private static final Cache<Elements> calledDomElements = new Cache <>();
  
  /**
   * clean all caches used by this class
   */
  public static void purgeCaches(){
    calledDomElements.purge();
    calledDomElement.purge();
    JsoupWrapper.purgeCaches();
  }
  /**
   * actually perform the HTML calling and get the first element based on the given selectors
   *
   * @param doc      the Document to be parsed
   * @param selector the CSS-style selector of the desired Element
   * @return the desired element
   */
  public static Element parseElement (Document doc, String selector) {
    return (null == doc || 0 == selector.length() ) ? null : JsoupWrapper.jsoupFirst(doc, selector);
  }
    /**
   * wrapper for parseElement using Document
   *
   * @param url      the requested url
   * @param selector the CSS-style selector of the desired Element
   * @return the desired element
   */
    public static Element parseElement (String url, String selector) {
    String[] identifier = {url, selector};
    CacheAction<Element> action = new ParseElement(url, selector);
    return calledDomElement.get(identifier, action);
  }
  

  
  /**
   * actually perform the HTML calling and get the first element based on the given selectors
   *
   * @param doc      the Document to be parsed
   * @param selector the CSS-style selector of the desired Element
   * @return the desired element
   */
  public static Elements parseElements (Document doc, String selector) {
    return (null == doc || null == selector || 0 == selector.length() ) ? null : JsoupWrapper.jsoupAll(doc, selector);
  }
  
  /**
   * wrapper for parseElements using Document
   */
  public static Elements parseElements (String url, String selector) {
    
    String[] identifier = {url, selector};
    CacheAction<Elements> action = new ParseElements(url, selector);
    return calledDomElements.get(identifier, action);
  }
  
  /**
   * gets the base name of the file to be saved
   *
   * @param path       what came behind the domain in the requested url
   * @param domainName the name as defined in the domain node name attribute
   * @return an assembled download file base name
   */
  static String getImageDownloadBaseName(String path, String domainName) {
    StringBuilder fileName = new StringBuilder();
    NodeList fileNameFragmentNodes = FileNameFragment.fragments(domainName);
    int fileNameFragmentNodesLength = fileNameFragmentNodes.getLength();

    Logger.log("Parsing name fragments");

    // loop over name fragments from the config
    for (int i = 0; i < fileNameFragmentNodesLength; i++) {
      fileName.append(getImageNameFragment(domainName, path, i));
    }
    Logger.log("Done parsing name fragments");
  
    fileName = new StringBuilder(StringUtils.sanitiseFileName(fileName.toString()));
    return fileName.toString();
  }
  
  /**
   * gets an individual image name fragment for a specific image
   * TODO: Split
   */
  @SuppressWarnings("WeakerAccess")
  protected static String getImageNameFragment(String domainName, String path, int index){
    NodeList fileNameFragmentNodes = FileNameFragment.fragments(domainName);
    org.w3c.dom.Element fileNameFragmentNode = (org.w3c.dom.Element) fileNameFragmentNodes.item(index);
    Node fileNameData = FileNameFragment.data(domainName, index + 1);
    String fileNameDataType = fileNameData.getAttributes().getNamedItem("type").getNodeValue();
    String fileNameFragmentInnerConjunction = fileNameFragmentNode.getAttribute("inner_conjunction");
    String fileNameDataValue = fileNameData.getTextContent();
  
    // Conjunction
    String fileNameConjunction = "";
    try {
      fileNameConjunction = fileNameFragmentNode.getAttributes().getNamedItem("conjunction").getNodeValue();
    } catch (Exception e) { /*e.printStackTrace();*/ }
    
    
    Node fileNamePatternNode = FileNameFragment.pattern(domainName, index + 1);
    String fileNamePattern = Replace.CATCHALL_PATTERN;
    try {
      fileNamePattern = fileNamePatternNode.getTextContent();
    } catch (Exception e) {/* * /log("no pattern node content found.");e.printStackTrace();/*Everything is fine, nothing to see*/}
    String fileNameReplace = "$1";
    try {
      fileNameReplace = fileNamePatternNode.getAttributes().getNamedItem("replace").getNodeValue();
    } catch (Exception e) {/* * /log("no pattern node content found.");e.printStackTrace();/*Everything is fine, nothing to see*/}
  
  
  
    String fileNameFragment = "";
    String fileNameSelector;
    // each type of data source needs a special treatment
    if ("url".equals(fileNameDataType)) {
      Logger.log("parsing name from path '" + path + "' according to pattern '" + fileNamePattern + "'");
      fileNameFragment = path.replaceAll(fileNamePattern, fileNameReplace);
    } else {
      /*
      in: domainName, path, fileNameDataType, fileNamePattern, index
      */
      String domain = Domain.url(domainName);
      String requestUrl = domain + path;
      Logger.log("Target is no url. Calling " + requestUrl + " and parsing it regarding " + fileNameDataType + " on " + fileNamePattern);
      fileNameSelector = FileNameFragment.selector(domainName, index + 1);
      Elements fileNameElements = parseElements(requestUrl, fileNameSelector);
      int fileNameElementNum = fileNameElements.size();
      String fileNameFragmentPiece = "";
      for (int i = 0; i < fileNameElementNum; i++) {
        Element fileNameElement = fileNameElements.get(i);
        if ("html".equals(fileNameDataType)) {
          fileNameFragmentPiece = fileNameElement.html();
        } else if ("attr".equals(fileNameDataType)) {
          fileNameFragmentPiece = fileNameElement.attr(fileNameDataValue);
        }
        fileNameFragmentPiece = fileNameFragmentPiece.replaceAll(fileNamePattern, fileNameReplace);
        if (i > 0 && fileNameFragmentInnerConjunction.length() > 0 && fileNameFragmentPiece.length() > 0){
          fileNameFragment += fileNameFragmentInnerConjunction;
        }
        fileNameFragment += fileNameFragmentPiece;
      }
      Logger.log("fileNameSelector: " + fileNameSelector);

    }
    
    Logger.log(index);
    Logger.log("fileNameFragment: " + fileNameFragment);
    Logger.log("fileNameDataType: " + fileNameDataType);
    Logger.log("fileNameDataValue: " + fileNameDataValue);
    Logger.log("fileNamePattern: " + fileNamePattern);
    return fileNameConjunction + fileNameFragment;
  }
  
  /**
   * check if based on config and url a login request is necessary
   */
  public static boolean isLoginNecessary (String domainName, String requestUrl){
    String loginRequiredSelector = Login.loginRequiredSelector(domainName);
    try {
      calledDomElements.purge();
      Elements loginRequiredElements = parseElements(requestUrl, loginRequiredSelector);
      return loginRequiredElements.size() > 0;
    } catch (NullPointerException e) {
      //e.printStackTrace();
    }
    return false;
  }
  
  /**
   * detects if a requested url contains an nsfw-post
   * @param domainName
   * @param requestUrl
   * @return
   */
  public static boolean isNSFW(String domainName, String requestUrl){
    String nsfwSelector   = Nsfw.selector(domainName);
    if ( null == nsfwSelector) {
      return false;
    }
    Node   nsfwPattern    = Nsfw.pattern(domainName);
    String pattern = null;
    if(null != nsfwPattern) {
      if(null != nsfwPattern.getTextContent()) {
        pattern = nsfwPattern.getTextContent();
      }
    }
    Node   nsfwDataType  = Nsfw.dataType(domainName);
    String type = null;
    String attributeName = null;
    if(null != nsfwDataType) {
      if(null != nsfwDataType.getFirstChild()) {
        attributeName = nsfwDataType.getFirstChild().getTextContent();
      }
      if(null != nsfwDataType.getAttributes()
              && null != nsfwDataType.getAttributes().getNamedItem("type")) {
        type = nsfwDataType.getAttributes().getNamedItem("type").getNodeValue();
      }
    }
    
    Element nsfwElement = parseElement(requestUrl, nsfwSelector);
    
    if(null != nsfwElement && null != type && type.equals("attr") && null != attributeName && null != pattern && !pattern.equals("") ){
      return nsfwElement.attr(attributeName).matches(pattern);
    }
    // TODO: support other types than attr - e.g. text content
    return false;
  }
  /**
   * get a download link for an image as defined in the config
   * TODO: Split
   *
   * @param domainName the name as defined in the domain node name attribute
   * @param requestUrl full url given as parameter to the program (cleaned up)
   * @param version    hd/sd
   * @return the parsed download url for the version
   */
  static String[] getImageDownloadUrls (String domainName, String requestUrl, String version) {
    try {
      String imageSelector = Image.selector(domainName, version);
      Node imageData = Image.data(domainName, version);
      String imageDataType = imageData.getAttributes().getNamedItem("type").getNodeValue();
      String imageDataValue = imageData.getTextContent();
      Node imagePatternNode = Image.pattern(domainName, version);
      String imagePattern = Replace.CATCHALL_PATTERN;
      try {
        imagePattern = imagePatternNode.getTextContent();
      } catch (Exception e) {/* * /log("no pattern node content found.");e.printStackTrace();/*Everything is fine, nothing to see*/}

      String imageReplace = "$1";
      try {
        imageReplace = imagePatternNode.getAttributes().getNamedItem("replace").getNodeValue();
      } catch (Exception e) {/* * /log("no replace attribute found.");e.printStackTrace();/*Everything is fine, nothing to see*/}
      Logger.log("imageSelector: " + imageSelector);
      Logger.log("imageDataType: " + imageDataType);
      Logger.log("imageDataValue: " + imageDataValue);
      String[] imageUrls = new String[0];
      try {
        if ("url".equals(imageDataType)){
          imageUrls = new String[1];
          imageUrls[0] = requestUrl;
        } else {
          Elements imageElements = parseElements(requestUrl, imageSelector);
          int numImageElements = imageElements != null ? imageElements.size() : 0;
          imageUrls = new String[numImageElements];
          for (int i = 0; i < numImageElements; i++) {
            Element imageElement = imageElements.get(i);
            if ("html".equals(imageDataType)) {
              imageUrls[i] = imageElement.html();
            } else if ("attr".equals(imageDataType)) {
              imageUrls[i] = imageElement.attr(imageDataValue);
            } else if ("contentattr".equals(imageDataType)) {
              imageUrls[i] = imageElement.childNode(0).attr(imageDataValue);
            }
          }
        }
        for (int i = 0; i < imageUrls.length; i++) {
          Logger.log(version + " Image url pre replace: " + imageUrls[i]);
          Logger.log(version + " image patter: " + imagePattern);
          Logger.log(version + " image replace: " + imageReplace);
          imageUrls[i] = imageUrls[i].replaceAll("\r|\n", "");
          imageUrls[i] = imageUrls[i].replaceAll(imagePattern, imageReplace);
          imageUrls[i] = StringUtils.urlPrependProtocol(imageUrls[i]);
          Logger.log(version + " Image url post replace: " + imageUrls[i]);
          Logger.log(version + " image URL: " + imageUrls[i]);
          String path = imageUrls[i].replaceAll("^((?:[^/]*/)+)([^.]+)(\\..*)$", "$1");
          String name = imageUrls[i].replaceAll("^((?:[^/]*/)+)([^.]+)(\\..*)$", "$2");
          name = URLEncoder.encode(name,"UTF-8").replaceAll("\\+", "%20");
          String ending = imageUrls[i].replaceAll("^((?:[^/]*/)+)([^.]+)(\\..*)$", "$3");
          imageUrls[i] = path+name+ending;
          Logger.log(version + " Image url post encoding: " + imageUrls[i]);
        }
      } catch (IOException e) {
        Logger.log(version + " image configured, but not found.");
      }
      return imageUrls;
    } catch (Exception e) {
      Logger.log("There seems to be no " + version + " version configured");
    }
    return null;
  }
  
  /**
   * the callable used for adding a missing element to the cache
   */
  static class ParseElement extends CacheAction<Element> {
    final String url;
    final String selector;

    ParseElement(String url, String selector) {
      this.url = url;
      this.selector = selector;
    }
    public Element action() throws Exception{
      Document doc = JsoupWrapper.jsoupRequest(url);
      Logger.log("parseElement: load "+url+ Cache.cacheDivisor+selector+" from remote.", true);
      return parseElement(doc, selector);
    }
  }
  
  /**
   * the callable used for adding missing elements to the cache
   */
  static class ParseElements extends CacheAction<Elements> {
    final String url;
    final String selector;

    ParseElements(String url, String selector) {
      this.url = url;
      this.selector = selector;
    }
    public Elements action() throws Exception{
      Document doc = JsoupWrapper.jsoupRequest(url);
      Logger.log("parseElements: load "+url+ Cache.cacheDivisor+selector+" from remote.", true);
      return parseElements(doc, selector);
    }
  }

}
