#! /bin/bash
set -o verbose
ARGS=""
for ARG in "$@"
do
  if [ "$ARG" = "love" ]
    then
      ARGS=$ARGS" http://thecodinglove.com/post/146065527308/when-my-code-is-compiling-but-the-clock-hits"
  elif [ "$ARG" = "xkcd" ]
    then
      ARGS=$ARGS" http://xkcd.com/681/"
  elif [ "$ARG" = "schiss" ]
    then
      ARGS=$ARGS" http://www.schisslaweng.net/beherrschung/"
  elif [ "$ARG" = "awkward" ]
    then
      ARGS=$ARGS" http://awkwardzombie.com/index.php?page=0&comic=061316"
  elif [ "$ARG" = "ponyhof" ]
    then
      ARGS=$ARGS" http://sarahburrini.com/wordpress/comic/endlich-strand/"
  elif [ "$ARG" = "phd" ]
    then
      ARGS=$ARGS" http://www.phdcomics.com/comics.php?f=1885"
  elif [ "$ARG" = "404" ]
    then
      ARGS=$ARGS" http://9gag.com/gag/a5Z2ARV"
  fi
done
cd target/classes/
echo "${ARGS}"
java -classpath ../lib/jsoup-1.9.2.jar:. MediaDownloader ${ARGS}
