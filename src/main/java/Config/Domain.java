package Config;

import Text.Replace;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Created by mmodrow on 10.07.2017.
 */
public class Domain extends ReadConfig {
  
  @SuppressWarnings("WeakerAccess")
  protected static final String DOMAIN_CFG_PATH = "domains.xml";
  
  @SuppressWarnings("WeakerAccess")
  protected static final Domain DOMAIN_INSTANCE = new Domain();
  
  protected Domain () {
    super();
  }
  
  static Domain getInstance () {
    return DOMAIN_INSTANCE;
  }
  
  /**
   * gets a domain from the config file by name
   */
  public static String fromName (String domainName) {
    if(null == domainName || 0 == domainName.length()) { return null; }
    NodeList nodeList = getInstance().getConfigPathElements("/domains/domain[@name='" + domainName + "']", configStatic());
    if (null == nodeList) {
      String domainAlias = nameFromAlias(domainName);
      nodeList = getInstance().getConfigPathElements("/domains/domain[@name='" + domainAlias + "']", configStatic());
    }
    if (null == nodeList ||0 == nodeList.getLength()){ return null; }
    return nodeList.item(0).getAttributes().getNamedItem("name").getTextContent();
  }
  
  /**
   * get a domain name by an alias
   */
  public static String nameFromAlias (String domainAlias){
    if(null == domainAlias || 0 == domainAlias.length()) { return null; }
    return getInstance().getConfigPathElementString("/domains/alias[@name='" + domainAlias + "']", configStatic());
  }
  
  /**
   * reads the configuration for parsable domains from the domain config xml
   *
   * @return XML-Document from the config file
   */
  public static org.w3c.dom.Document configStatic () {
    return getInstance().config();
  }
  
  /**
   * get the pattern to validate a domain's url
   */
  public static String pathPattern (String domainName){
    if(null == domainName || 0 == domainName.length()) { return null; }
    String pattern = getInstance().getConfigPathElementString("/domains/domain[@name='" + domainName + "']/pattern", configStatic());
    if(null == pattern) {
      pattern = Replace.CATCHALL_PATTERN;
    }
    return pattern;
  }
  
  /**
   * Reforms a path according to it's replacement string.
   */
  public static String pathPatternReplace (String domainName, String path){
    if(null == domainName || 0 == domainName.length()) { return null; }
    
    Node patternNode = getInstance()
            .getConfigPathElements("/domains/domain[@name='" + domainName + "']/pattern", configStatic())
            .item(0);
    
    if(null == patternNode) {
      return path;
    }
  
    Node replaceNode = patternNode.getAttributes().getNamedItem("replace");
    
    if(null == replaceNode) {
      return path;
    }
    
    String replaceString = replaceNode.getNodeValue();
    
    String pattern = pathPattern(domainName);
  
    return path.replaceAll(pattern, replaceString);
  }
  
  /**
   * get a domain's url
   */
  public static String url (String domainName){
    if(null == domainName || 0 == domainName.length()) { return null; }
    return getInstance().getConfigPathElementString("/domains/domain[@name='" + domainName + "']/url", configStatic());
  }
  
  /**
   * get a domain's use query
   */
  public static boolean useQuery (String domainName){
    if(null == domainName || 0 == domainName.length()) { return false; }
    String useQuery = getInstance().getConfigPathElementString("/domains/domain[@name='" + domainName + "']/useQuery", configStatic());
    return null != useQuery && useQuery.equals("true");
  }
  
  /**
   * reads the configuration for parsable domains from the domain config xml
   *
   * @return XML-Document from the config file
   */
  public org.w3c.dom.Document config () {
    return getConfig(Domain.DOMAIN_CFG_PATH);
  }
}
