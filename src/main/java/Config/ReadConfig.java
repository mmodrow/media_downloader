package Config;

import Caching.Cache;
import Caching.CacheAction;
import IO.FileSystem;
import Notification.FlashBack;
import Text.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import java.io.File;
import java.io.IOException;

/**
 * Created by mmodrow on 18.08.2016.
 * Everything that either is a configuration in itself or refers to the config xml files
 */
@SuppressWarnings("WeakerAccess")
public class ReadConfig {
  
  
  @SuppressWarnings("WeakerAccess")
  protected static final ReadConfig READ_CONFIG_INSTANCE = new ReadConfig();
  @SuppressWarnings("WeakerAccess")
  protected static final XPath xPath = XPathFactory.newInstance().newXPath();
  
  
  // Config.ReadConfig caches
  private final Cache<NodeList> xPathCache = new Cache <>();
  private final Cache<Document> configCache = new Cache <>();
  
  protected ReadConfig () {}
  @SuppressWarnings("SameReturnValue")
  static ReadConfig getInstance () {
    return READ_CONFIG_INSTANCE;
  }
  
  /**
   * get a NodeList from an expression and a document
   *
   * @param expression xPath-Expression
   * @param doc        json GET response Document
   * @return a list of Nodes
   */
  static NodeList callXPath(String expression, org.w3c.dom.Document doc) {
    if (null == doc || null == expression || 0 == expression.length()) {
      return null;
    }
    ReadConfig instance = getInstance();
    Cache<NodeList> xPathCache = instance.xPathCache;
    String[] selector = {expression, doc.getBaseURI()};
    return xPathCache.get(selector, new CallXPath(expression, doc));
  }
  
  
  /**
   * the callable used for adding missing a config file to the cache
   */
  static class CallXPath extends CacheAction<NodeList> {
    final String expression;
    final org.w3c.dom.Document doc;

    CallXPath(String expression, org.w3c.dom.Document doc) {
      this.expression = expression;
      this.doc = doc;
    }
    public NodeList action() throws Exception{
      
    NodeList result = (NodeList) xPath.compile(expression).evaluate(doc, XPathConstants.NODESET);
    Logger.log("created cache value for "+expression, true);
    return result;
    }
  }
  
  /**
   * gets the string that corresponds to a request on a given Document
   */
  protected NodeList getConfigPathElements (String xPathRequestString, org.w3c.dom.Document document){
    NodeList nodeList = ReadConfig.callXPath(xPathRequestString, document);
    return nodeList.getLength() > 0 ? nodeList:null;
  }
  
  protected Node getConfigPathFirstElement (String xPathRequestString, org.w3c.dom.Document document){
    NodeList nodeList = getConfigPathElements(xPathRequestString, document);
    return (null != nodeList) ? nodeList.item(0) : null;
  }
  
  /**
   * gets the string that corresponds to a request on a given Document
   */
  protected String getConfigPathElementString(String xPathRequestString, org.w3c.dom.Document document){
    NodeList nodeList = ReadConfig.callXPath(xPathRequestString, document);
    if (nodeList.getLength() > 0) {
      return nodeList.item(0).getTextContent();
    } else {
      return null;
    }
  }
  
  
  /**
   * reads the configuration for parsable domains from the domain config xml
   *
   * @return XML-Document from the config file
   */
  protected org.w3c.dom.Document getConfig(String fileName) {
    CacheAction<Document> action = new GetConfig(fileName);
    return configCache.get(fileName, action);
  }
  
  
  /**
   * the callable used for adding missing a config file to the Caching.Cache
   */
  private static class GetConfig extends CacheAction<org.w3c.dom.Document> {
    final String fileName;
  
    /**
     * Constructor
     * @param fileName The name of the file to be read into cache.
     */
    GetConfig(String fileName) {
      this.fileName = fileName;
    }
  
    /**
     * Reads the set file and parses it into a DOM document.
     * @return A DOM document, that is parsed from the read file.
     * @throws Exception
     */
    public org.w3c.dom.Document action() throws Exception{
      try{
        File inputFile = FileSystem.getFile(fileName);
        org.w3c.dom.Document doc = parseXmlFile(inputFile);
        doc.getDocumentElement().normalize();
        Logger.log("created Caching.Cache for "+fileName, true);
        return doc;
      } catch (IOException e) {
        FlashBack.errorFlash();
        Logger.log("Config.ReadConfig file not found.");
        return null;
      }
    }
  }
  
  /**
   * Performs the parsing of a given file to an DOM document.
   * @param inputFile Read file
   * @return The parse DOM document.
   * @throws ParserConfigurationException
   * @throws IOException The file may be unreadable for some reason.
   * @throws SAXException
   */
  protected static org.w3c.dom.Document parseXmlFile(File inputFile) throws ParserConfigurationException, IOException, SAXException {
    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
    
    return dBuilder.parse(inputFile);
  }
}