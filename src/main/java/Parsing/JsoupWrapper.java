package Parsing;

import Caching.Cache;
import Caching.CacheAction;
import IO.HttpRequest;
import Text.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * wraps all the used JSOUP functionality and the dom-cache
 */
public class JsoupWrapper {

  private static final Cache<Document> calledDom = new Cache<>();

  /**
   * clean all caches used by this class
   */
  public static void purgeCaches(){
    calledDom.purge();
  }
  
  /**
   * get first element from a Document by a selector-string
   *
   * @param doc      json GET response Document
   * @param selector The CSS-style selector for the desired element
   * @return a dom element
   */
  @SuppressWarnings("WeakerAccess")
  protected static Elements jsoupAll(Document doc, String selector) {
    if (selector.length() < 1) {
      return null;
    }
    
    return doc.select(selector);
  }

  /**
   * get first element from a Document by a selector-string
   *
   * @param doc      json GET response Document
   * @param selector The CSS-style selector for the desired element
   * @return a dom element
   */
  @SuppressWarnings("WeakerAccess")
  protected static Element jsoupFirst(Document doc, String selector) {
    if (selector.length() < 1) {
      return null;
    }
    
    Elements elems = doc.select(selector);
    return elems.first();
  }

  /**
   * perform a jsoup request by url
   *
   * @param url the url to be requested
   * @return Document
   */
  @SuppressWarnings("WeakerAccess")
  public static Document jsoupRequest(String url) {
    if (null == url || url.length() < 1) {
      return null;
    }
    
    return calledDom.get(url, new JsoupRequest(url));
  }
  
  /**
   * the callable used for adding missing elements to the cache
   */
  static class JsoupRequest extends CacheAction<Document> {
    final String url;

    JsoupRequest(String url) {
      this.url = url;
    }
    public Document action() throws Exception {
      Document doc = Jsoup.connect(url).validateTLSCertificates(false).userAgent(HttpRequest.DEFAULT_USER_AGENT).timeout(HttpRequest.DEFAULT_TIMEOUT).get();
      Logger.log("jsoupRequest: load "+url+" from remote.", true);
      return doc;
    }
  }
}