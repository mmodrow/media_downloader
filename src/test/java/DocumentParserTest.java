import Parsing.DocumentParser;
import Parsing.JsoupWrapper;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.*;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

import static org.junit.Assert.*;

public class DocumentParserTest extends DocumentParser
{
  private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
  private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
  
  private Document document;
  @Before
  public void setUpStreams() {
    System.setOut(new PrintStream(outContent));
    System.setErr(new PrintStream(errContent));
  }
  
  @Before
  public void setUpDocuments() throws IOException {
    File input = new File(TestUtils.getTestInputDirectory() + "document01.html");
    document = Jsoup.parse(input, "UTF-8");
  }
  
  @After
  public void cleanUpStreams() {
    System.setOut(null);
    System.setErr(null);
  }
  
  @After
  public void cleanUpDocuments() {
    document = null;
  }
  
  @BeforeClass
  public static void oneTimeSetUp() {
    // one-time initialization code
  }
  
  @AfterClass
  public static void oneTimeTearDown() {
    TestUtils.deleteTestData();
  }
  
  @Before
  public void cleanupFiles(){
    TestUtils.deleteTestData();
  }
  
  
  @Before
  public void setUp() {
    //setup for each individual test
  }
  
  @After
  public void tearDown() {
    //de-setup for each individual test
  }
  
  /**
   * example of testing document parsing
   */
  @Test
  public void parseElementTitleTest() throws IOException {
    Element e = parseElement(document, ".testDiv>a>.testDiv__image");
    assertNotNull(e);
    String title = e.attr("title");
    assertEquals("imageTitle", title);
  }

  @Test
  public void parseElementHtmlTest() throws IOException {
    Element e = parseElement(document, ".testP--other>.testP__span");
    assertNotNull(e);
    String elementBody = e.html();
    assertEquals("OTHER TEST SPAN CONTENT", elementBody);
  }

  @Test
  public void parseElementFromURlHtmlTest() throws IOException {
    String url = TestUtils.remoteTestInputDirectory + "document01.html";
    Element e = parseElement(url, ".testP--other>.testP__span");
    assertNotNull(e);
    String elementBody = e.html();
    assertEquals("OTHER TEST SPAN CONTENT", elementBody);
  }
  
  @Test
  public void parseElementsHtmlTest() throws IOException {
    Elements e = parseElements(document, ".testP>.testP__span");
    assertNotNull(e);
    String elementBody = e.html();
    assertEquals("TEST SPAN CONTENT\nOTHER TEST SPAN CONTENT\nTHIRD TEST SPAN CONTENT", elementBody);
  }
    
  @Test
  public void parseElementsFromURlHtmlTest() throws IOException {
    String url = TestUtils.remoteTestInputDirectory + "document01.html";
    Elements e = parseElements(url, ".testP>.testP__span");
    assertNotNull(e);
    String elementBody = e.html();
    assertEquals("TEST SPAN CONTENT\nOTHER TEST SPAN CONTENT\nTHIRD TEST SPAN CONTENT", elementBody);
  }
  
  @Test
  public void parseElementsEmptySelectorTest() throws IOException {
    Elements e = parseElements(document, "");
    assertNull(e);
  }
  
  @Test
  public void parseElementsNullSelectorTest() throws IOException {
    Elements e = parseElements(document, null);
    //noinspection ConstantConditions
    assertNull(e);
  }
  
  
  @Test
  public void jsoupRequestNullTest() throws IOException {
    assertNull(JsoupWrapper.jsoupRequest(null));
  }
}