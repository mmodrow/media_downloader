package Text;

/**
 * Created by mmodrow on 18.08.2016.
 * All misc String operations that are specific to this Project
 */
public abstract class StringUtils {
  private static final String[] PATTERN_URL_PREPEND_PROTOCOL = {"^(//)", "http:$1"};
  
  /**
   * makes sure a string is valid as a filename
   * @return sanitised filename
   */
  public static String sanitiseFileName (String input) {
    if (null == input) {
      return "";
    }
    
    String output = Replace.replaceUmlauts(input);
    output = Replace.removeNonUmlautDiacritics(output);
    output = Replace.recreateSwearing(output);
    output = output.replaceAll("_", " ");
    output = Replace.replaceContractions(output);
    output = Replace.removeIllegalCharacters(output);
    output = Replace.replaceBadCharactersByUnderscore(output);
    output = output.replaceAll("^_*(.*?)_*$", "$1");
    output = output.replaceAll("_+", "_");
    return output;
  }
  
  /**
   * prepends url to "//www.domain.tld"-Links
   */
  public static String urlPrependProtocol (String url) {
    return url.replaceAll(PATTERN_URL_PREPEND_PROTOCOL[0], PATTERN_URL_PREPEND_PROTOCOL[1]);
  }
  
  /**
   * counts the lines divided by line breaks in a string
   */
  public static int countLines (String str) {
    String[] lines = str.split("\r\n|\r|\n");
    return lines.length;
  }
  
  /**
   * Checks if any char from an array is contained within a String
   * @link http://stackoverflow.com/a/8995988
   */
  public static boolean stringContainsItemFromList(String inputString, @SuppressWarnings("SameParameterValue") char[] items)
  {
    for (char item : items) {
      if (inputString.indexOf(item) != -1) {
        return true;
      }
    }
    return false;
  }
}