import IO.FileSystem;
import IO.HttpRequest;
import org.junit.*;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.PrintStream;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class HttpRequestTest extends HttpRequest
{
  private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
  private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
  
  
  @SuppressWarnings("FieldCanBeLocal")
  private static long timestamp;
  private static String tempDir;
  @SuppressWarnings("FieldCanBeLocal")
  private static String timeDir;
  
  
  private void setUpStreams () {
    System.setOut(new PrintStream(outContent));
    System.setErr(new PrintStream(errContent));
  }
  
  private void cleanUpStreams () {
    System.setOut(null);
    System.setErr(null);
  }
  
  @BeforeClass
  public static void oneTimeSetUp() {
    // one-time initialization code
  }
  
  @AfterClass
  public static void oneTimeTearDown() {
    TestUtils.deleteTestData();
  }
  
  private void cleanupFiles (){
    TestUtils.deleteTestData();
  }
  
  
  @Before
  public void setUp() {
    //setup for each individual test
    cleanupFiles();
    setUpStreams();
    
   
    timestamp = System.currentTimeMillis();
    tempDir = TestUtils.getTestTempDirectory();
    timeDir = tempDir+timestamp;
    FileSystem.createDirIfNeeded(timeDir);
  }
  
  @After
  public void tearDown() {
    //de-setup for each individual test
    cleanUpStreams();
  }
  
  
  /**
   * test the availability of urls.
   * keep in mind: this test will fail if you have no internet connectivity!
   */
  @Test
  public void isUrlReachable200Test() {
    assertTrue(isUrlReachable("https://docs.oracle.com/javase/7/docs/api/"));
  }
  @Test
  public void isUrlReachable404Test() {
    assertFalse(isUrlReachable("https://docs.oracle.com/javase/7/docs/api/sithethathopefullyneverwillexist"));
  }
  @Test
  public void isUrlReachableEmptyTest() {
    assertFalse(isUrlReachable(""));
  }
  @Test
  public void isUrlReachableNullTest() {
    assertFalse(isUrlReachable(null));
  }
  
  @Test
  public void downloadValidDottedNoConflictTest() throws Exception {
    String urlToLoad = TestUtils.remoteTestInputDirectory+"document01.html";
    String targetPath = tempDir+"someSource.htm".replaceAll("^\\.*/", "."+File.separator);
    @SuppressWarnings("ConstantConditions") File downloadedFile = new File(download(urlToLoad, targetPath));
    assertTrue(downloadedFile.exists());
  }
  
  @Test
  public void downloadValidNotDottedNoConflictTest() {
    String urlToLoad = TestUtils.remoteTestInputDirectory+"document01.html";
    String targetPath = tempDir+"someSource.htm".replaceAll("^\\.*[/\\\\]", "");
    boolean downloadSuccessful = false;
    try {
      @SuppressWarnings("ConstantConditions") File downloadedFile = new File(download(urlToLoad, targetPath));
      assertTrue(downloadedFile.exists());
      downloadSuccessful = true;
    } catch ( Exception e){
      // nothing
    }
    assertTrue(downloadSuccessful);
  }
}