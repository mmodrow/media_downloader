package Config;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Created by mmodrow on 10.07.2017.
 */
public class FileNameFragment extends ReadConfig {
  
  @SuppressWarnings("WeakerAccess")
  protected static final FileNameFragment FILE_NAME_FRAGMENT_INSTANCE = new FileNameFragment();
  
  protected FileNameFragment () {
    super();
  }
  
  static FileNameFragment getInstance () {
    return FILE_NAME_FRAGMENT_INSTANCE;
  }
  
  /**
   * returns a NodeList of filename fragments for a given domain
   */
  public static NodeList fragments (String domainName) {
    if(null == domainName || 0 == domainName.length()) { return null; }
    return getInstance().getConfigPathElements("/domains/domain[@name='" + domainName + "']/fileName/fragment", Domain.configStatic());
  }
  
  /**
   * returns a NodeList of filename fragments for a given domain
   */
  public static Node data (String domainName, int fragmentNumber) {
    if(null == domainName || 0 == domainName.length()) { return null; }
    String xPathRequestString = "/domains/domain[@name='" + domainName + "']/fileName/fragment[" + fragmentNumber + "]/data";
    return getInstance().getConfigPathFirstElement(xPathRequestString, Domain.configStatic());
  }
  
  /**
   * returns a NodeList of filename fragments for a given domain
   */
  public static Node pattern (String domainName, int fragmentNumber) {
    if(null == domainName || 0 == domainName.length()) { return null; }
    String xPathRequestString = "/domains/domain[@name='" + domainName + "']/fileName/fragment[" + fragmentNumber + "]/pattern";
    return getInstance().getConfigPathFirstElement(xPathRequestString, Domain.configStatic());
  }
  
  /**
   * get the selector of a specific file name fragment
   */
  public static String selector (String domainName, int fragmentNumber) {
    if(null == domainName || 0 == domainName.length()) { return null; }
    String xPathRequestString = "/domains/domain[@name='" + domainName + "']/fileName/fragment[" + fragmentNumber + "]/selector";
    return getInstance().getConfigPathElementString(xPathRequestString, Domain.configStatic());
  }
}
