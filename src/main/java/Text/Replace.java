package Text;

/**
 * Replaces and removes portions of Text like illegal chars.
 * Created by mmodrow on 09.07.2017.
 */
public class Replace {
  private static final String PATTERN_TO_REPLACE_BY_UNDERSCORE = "[^a-zA-Z0-9,-]";
  public static final String CATCHALL_PATTERN = "^(.*?)$";
  private static final String PATTERN_TO_REMOVE = "[´`'\"’.‘]";
  
  /**
   * Removed all characters that are not allowed.
   */
  public static String removeIllegalCharacters (String input) {
    return input.replaceAll(PATTERN_TO_REMOVE, "");
  }
  
  /**
   * Replaces all bad characters by underscores.
   */
  public static String replaceBadCharactersByUnderscore (String input) {
    return input.replaceAll(PATTERN_TO_REPLACE_BY_UNDERSCORE, "_");
  }
  
  /**
   * Get rid of all those nasty contractions.
   */
  public static String replaceContractions (String input) {
    // I don´t care, which quote you`d use
    String quotes = "[`´'’‘]";
    // word boundary
    String w = "\\b";
    
    //pronouns
    String she = w + "(S|s)he" + quotes;
    String he = w + "(H|h)e" + quotes;
    String we = w + "(W|w)e" + quotes;
    String you = w + "(Y|y)ou" + quotes;
    String I = w + "I" + quotes;
    String they = w + "(T|t)ey" + quotes;
    
    //suffixes
    String t = "t" + w;
    String s = "s" + w;
    String d = "d" + w;
    String ll = "ll" + w;
    String ve = "ve" + w;
    String re = "re" + w;
  
    //noinspection UnnecessaryLocalVariable
    String output = input.replaceAll("(A|a)ren" + quotes + "t", "$1re not")
            .replaceAll(w + "(C|c)an" + quotes + t, "$1annot")
            .replaceAll(w + "(C|c)ouldn" + quotes + t, "$1ould not")
            .replaceAll(w + "(D|d)on" + quotes + t, "$1o not")
            .replaceAll(w + "(D|d)oesn" + quotes + t, "$1oes not")
            .replaceAll(I + "m" + w, "I am")
            .replaceAll(I + ll, "I will")
            .replaceAll(w + "(||i)t" + quotes + s, "$1t is")
            .replaceAll(w + "isn" + quotes + t, "is not")
            .replaceAll(he + s, "$1e has")
            .replaceAll(he + d, "$1e would")
            .replaceAll(he + ll, "$1e will")
            .replaceAll(she + s, "$1he has")
            .replaceAll(she + d, "$1he would")
            .replaceAll(she + ll, "$1he will")
            .replaceAll("(C|c)ould" + quotes + ve, "$1ould have")
            .replaceAll("(S|s)hould" + quotes + ve, "$1hould have")
            .replaceAll("(S|s)houldn" + quotes + t, "$1hould not")
            .replaceAll(they + re, "$1hey are")
            .replaceAll(they + ve, "$1hey have")
            .replaceAll(they + ll, "$1hey will")
            .replaceAll(we + re, "$1e are")
            .replaceAll(we + ve, "$1e have")
            .replaceAll(we + ll, "$1e will")
            .replaceAll("(W|w)eren" + quotes + t, "$1ere not")
            .replaceAll("(W|w)ouldn" + quotes + t, "$1ould not")
            .replaceAll("(W|w)on" + quotes + t, "$1ill not")
            .replaceAll(you + re, "$1ou are")
            .replaceAll(you + ve, "$1ou have")
            .replaceAll(you + ll, "$1ou will")
            .replaceAll(you + d, "$1ou would");
    return output;
  }
  
  /**
   * Replaces umlauts with their common transcription within a string.
   * @return input string with all umlauts replaced
   * @link http://stackoverflow.com/a/32696479
   */
  public static String replaceUmlauts (String input) {
    //replace all lower Umlauts
    String output = input.replace("ü", "ue")
            .replace("ö", "oe")
            .replace("ä", "ae")
            .replace("ß", "ss");
  
    //first replace all capital umlauts in a non-capitalized context (e.g. Übung)
    output = output.replaceAll("Ü(?=[a-zäöüß ])", "Ue")
            .replaceAll("Ö(?=[a-zäöüß ])", "Oe")
            .replaceAll("Ä(?=[a-zäöüß ])", "Ae");
  
    //now replace all the other capital umlauts
    output = output.replace("Ü", "UE")
            .replace("Ö", "OE")
            .replace("Ä", "AE");
  
    return output;
  }
  
  /**
   * Removes diacritics from non-latin letters that cannot be converted like umlauts (ü -> ue).
   */
  public static String removeNonUmlautDiacritics (String input) {
    //noinspection UnnecessaryLocalVariable
    String output = input.replaceAll("[àáâãå]", "a")
            .replaceAll("[ÀÁÂÃÅ]", "A")
            .replaceAll("[èéêë]", "e")
            .replaceAll("[ÈÉÊË]", "E")
            .replaceAll("[ÌÍÎÏ]", "I")
            .replaceAll("[ìíîï]", "i")
            .replaceAll("[òóôõ]", "o")
            .replaceAll("[ÒÓÔÕ]", "O")
            .replaceAll("[ùúû]", "u")
            .replaceAll("[ÙÚÛ]", "U");
    return output;
  }
  
  /**
   * Reestablish jumbled swear words messing up file names.
   */
  public static String recreateSwearing (String input) {
    String output = input.replaceAll("(f|F)[^a-zA-Z](c|[^a-zA-Z])k", "$1uck");
    output = output.replaceAll("(F)[^a-zA-Z](C|[^a-zA-Z])K", "FUCK");
    output = output.replaceAll("\\*{4}", "shit");
    output = output.replaceAll("(B|b)[^a-zA-Z]tch", "$1itch");
    
    return output;
  }
}