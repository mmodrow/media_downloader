/*
  Printing part based on the GUI console code from RJHM van den Bergh <rvdb@comweb.nl>
 
  @link http://www.comweb.nl/java/Console/Console.html
 */

import Notification.Flash;
import Parsing.DownloadLogic;
import Text.Logger;
import UI.DropTarget;
import UI.WindowFrame;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;


/**
 * The type Media downloader.
 */
class MediaDownloader extends WindowAdapter implements WindowListener, ActionListener{
  private final WindowFrame frame;
  
  
  /**
   * The entry point of application.
   *
   * @param args the input arguments
   */
  public static void main(String[] args) {
    boolean exit = true;
    try {
      if (0 == args.length) {
          new MediaDownloader();
          exit = false;
      } else {
        for(String url : args) {
          DownloadLogic.processUrl(url);
        }
      }
    } catch (java.awt.HeadlessException e) {
      exit = true;
      Logger.log(e.getMessage(), true, true);
    }
    if (exit){
      System.exit(0);
    }
  }

  /**
   * Constructor
   */
  @SuppressWarnings("WeakerAccess")
  public MediaDownloader() {
    // create all components and add them
    frame = new WindowFrame();
  
    Flash.defaultTarget = DropTarget.getTextArea();

    JButton clearButton = new JButton("clear");
    JButton copyButton = new JButton("copy");
  
  
    clearButton.addActionListener(this);
    copyButton.addActionListener(this);
    frame.addWindowListener(this);
    frame.getContentPane().add(clearButton, BorderLayout.SOUTH);
    frame.getContentPane().add(copyButton, BorderLayout.NORTH);
    // signals the Threads that they should exit
    DropTarget.quit = false;
  }


  /**
   * Window closed event handler.
   */
  public synchronized void windowClosed(WindowEvent evt) {
    DropTarget.quit = true;
    // stop all threads
    this.notifyAll();
    DropTarget.getInstance().closeReaders();
    System.exit(0);
  }

  /**
   * Window closing event handler.
   */
  @Override
  public synchronized void windowClosing(WindowEvent evt) {
    // default behaviour of JFrame
    frame.setVisible(false);
    frame.dispose();
  }
  
  /**
   * General user action event handler.
   */
  @Override
  public synchronized void actionPerformed(ActionEvent evt) {
    if (evt.getActionCommand().equals("copy")) {
      DropTarget.getInstance().copySelection();
    } else if (evt.getActionCommand().equals("clear")) {
      DropTarget.getTextArea().setText("");
    }
  }
}