package IO;

import Text.Logger;
import Text.StringUtils;

import javax.swing.*;
import java.awt.*;
import java.io.File;

/**
 * Created by mmodrow on 20.08.2016.
 * Handles all things relating local file system files & directories that are no config xml files
 */
public abstract class FileSystem {
  /** @link http://stackoverflow.com/a/894133 */
  private static final char[] ILLEGAL_CHARACTERS = {'\n', '\r', '\t', '\0', '\f', '`', '?', '*', '<', '>', '|', '\"' };//, ':'
  
  /**
   * create missing directory to a file path
   *
   */
  public static boolean createDirIfNeeded (String path) {
    if (null == path || path.equals("") || StringUtils.stringContainsItemFromList(path, ILLEGAL_CHARACTERS)) {return false;}
    
    String directory = path.substring(0,Math.max(path.lastIndexOf(File.separator), 0));
    if (directory.equals("")) {return false;}
    File file = new File(directory);

 
    /*
     * exists() method tests whether the file or directory denoted by this
     * abstract pathname exists or not accordingly it will return TRUE /
     * FALSE.
     */
    if (!file.exists()) {
      /*
       * mkdirs() method creates the directory mentioned by this abstract
       * pathname including any necessary but nonexistent parent
       * directories.
       *
       * Accordingly it will return TRUE or FALSE if directory created
       * successfully or not. If this operation fails it may have
       * succeeded in creating some of the necessary parent directories.
       */
      if (file.mkdirs()) {
        Logger.log("Directory " + directory + " successfully created", true, true);
        return true;
      } else {
        Logger.log("Failed to create directory " + directory + ".", true, true);
        return false;
      }
    } else {
      Logger.log("Directory " + directory + " is present already.");
      return true;
    }

  }
  
  /**
   * rename download target path if needed
   */
  public static String dlPathNoConflict (String path) throws NullPointerException {
    if (null == path){ return ""; }
    File file = new File(path);
    String newPath = path;
    String directory = path.substring(0,path.lastIndexOf(File.separator)+1);
    String fileName = path.substring(path.lastIndexOf(File.separator)+1);
    int dotPos = fileName.lastIndexOf(".");
    String fileBaseName = dotPos != -1 ? fileName.substring(0, dotPos) : fileName;
    if (file.exists() || fileBaseName.equals("")) {
      String fileEnding = fileName.substring(Math.max(0,dotPos));
      newPath = directory + fileBaseName + "_" + (System.currentTimeMillis() / 1000L) + fileEnding;
      if(fileBaseName.equals("")){
        Logger.log("Filename was empty.\nIt was set to " + newPath + ".", true, true);
      } else {
        Logger.log("File " + path + " already exists.\nRenaming it to " + newPath + ".", true, true);
      }
    }
    return newPath;
  }
  
  /**
   * limit file name length
   */
  public static String limitFileNameLength (String fileName, int suffixLength) {
    String processedFileName = fileName;
    int maxLength = 255;      // windows file system limit for file names
    int timeCodeLength = 11;  // space reserved for time code if conflict arises
    int fileEndingLength = 5; // space reserved for file ending incl. dot
    int fileNameLengthLimit = maxLength-timeCodeLength-fileEndingLength-suffixLength;
    if (processedFileName.length() > fileNameLengthLimit) {
      processedFileName = processedFileName.substring(0, fileNameLengthLimit);
    }
    return processedFileName;
  }
  
  /**
   * gets the size of a file that can be found by the given file path
   */
  public static long getFileSize(String file) throws NullPointerException {
    File f = new File(file);
    Logger.log("Size: " + f.length());
    return f.length();
  }
  
  public static boolean isImage(String file){
    if(null == file || file.equals("")) { return false; }
    Image image = new ImageIcon(file).getImage();
    return !(image.getWidth(null) == -1);
  }
  
  /**
   * try to get a file and fall back to parent dir if necessary
   *
   * @return file or null
   */
  public static File getFile (String fileName) {
    File inputFile = new File(fileName);
    if (!inputFile.exists()) {
      inputFile = new File("../" + fileName);
    }
    if (!inputFile.exists()) {
      Logger.log("File '" + fileName + "' not found.");
    }
    return inputFile.exists() ? inputFile : null;
  }
}
