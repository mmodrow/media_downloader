package Config;

import org.w3c.dom.Node;

/**
 * Created by mmodrow on 10.07.2017.
 */
public class Image extends ReadConfig {
  
  @SuppressWarnings("WeakerAccess")
  protected static final Image IMAGE_INSTANCE = new Image();
  protected static final String DOWNLOAD_PATH_CFG_PATH = "download_paths.xml";
  
  protected Image () {
    super();
  }
  
  static Image getInstance () {
    return IMAGE_INSTANCE;
  }
  
  /**
   * get the folder to save the images to
   */
  public static String folder (String domainName){
    if(null == domainName || 0 == domainName.length()) { return null; }
    String xPathRequestString = "/domains/domain[@name='" + domainName + "']/folder";
    return getInstance().getConfigPathElementString(xPathRequestString, configStatic());
  }
  
  /**
   * get the selector for the image on the page
   */
  public static String selector (String domainName, String version) {
    if(null == domainName || 0 == domainName.length()) { return null; }
    String xPathRequestString = "/domains/domain[@name='" + domainName + "']/images/image[@version='" + version + "']/selector";
    return getInstance().getConfigPathElementString(xPathRequestString, Domain.configStatic());
  }
  
  /**
   * get the data type for the image on the page
   */
  public static Node data (String domainName, String version) {
    if(null == domainName || 0 == domainName.length()) { return null; }
    String xPathRequestString = "/domains/domain[@name='" + domainName + "']/images/image[@version='" + version + "']/data";
    return getInstance().getConfigPathFirstElement(xPathRequestString, Domain.configStatic());
  }
  
  /**
   * get the selector for the image on the page
   */
  public static Node pattern (String domainName, String version) {
    if(null == domainName || 0 == domainName.length()) { return null; }
    String xPathRequestString = "/domains/domain[@name='" + domainName + "']/images/image[@version='" + version + "']/pattern";
    return getInstance().getConfigPathFirstElement(xPathRequestString, Domain.configStatic());
  }
  
  public static org.w3c.dom.Document configStatic (){
    return getInstance().getDownloadConfig();
  }
  
  /**
   * reads the configuration for parsable domains from the domain config xml
   *
   * @return XML-Document from the config file
   */
  public org.w3c.dom.Document getDownloadConfig() {
    return getConfig(DOWNLOAD_PATH_CFG_PATH);
  }
}
