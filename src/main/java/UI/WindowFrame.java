package UI;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

/**
 * Created by mmodrow on 29.06.2016.
 * handles the GUI
 */
public class WindowFrame extends JFrame implements WindowListener {
  
  /**
   * Constructor
   */
  public WindowFrame() {
    super("MediaDownloader");
    setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    Dimension frameSize = new Dimension(screenSize.width / 2, screenSize.height / 2);
    int x = frameSize.width / 2;
    int y = frameSize.height / 2;
    
    setBounds(x, y, 500, 250);
    getContentPane().setLayout(new BorderLayout());
    getContentPane().add(new JScrollPane(DropTarget.getTextArea()), BorderLayout.CENTER);
    setVisible(true);
  }
  
  /**
   * Invoked the first time a window is made visible.
   */
  @Override
  public void windowOpened(WindowEvent e) {
  }

  /**
   * Invoked when the user attempts to close the window from the window's system menu.
   */
  @Override
  public void windowClosing(WindowEvent e) {
    // can do cleanup here if necessary
  }

  /**
   * Invoked when a window has been closed as the result of calling dispose on the window.
   */
  @Override
  public void windowClosed(WindowEvent e) {
    dispose();
    System.exit(0);
  }

  /**
   * Invoked when the Window is set to be the active Window.
   */
  @Override
  public void windowActivated(WindowEvent e) {
  }

  /**
   * Invoked when a Window is no longer the active Window.
   */
  @Override
  public void windowDeactivated(WindowEvent e) {
  }

  /**
   * Invoked when a window is changed from a minimized to a normal state.
   */
  @Override
  public void windowDeiconified(WindowEvent e) {
  }

  /**
   * Invoked when a window is changed from a normal to a minimized state.
   */
  @Override
  public void windowIconified(WindowEvent e) {
  }
}