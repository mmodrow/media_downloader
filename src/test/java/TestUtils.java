import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

import static java.nio.file.FileVisitResult.CONTINUE;
import static java.nio.file.FileVisitResult.TERMINATE;

/**
 * Created by mmodrow on 14.11.2016.
 * Misc utility functions for testing as well as testing parameters like pathes etc
 */
class TestUtils {
  //everything test goes in here
  private static final String directoryPrefix = "target" + File.separator + "test-classes" + File.separator;
  private static final String testTempDirectory = "temp" +File.separator;
  private static final String testInputDirectory = "testInput" +File.separator;
  // sadly jsoup only supports http/s requests, not the local file system. Thus the referenceto the host project.
  // update the reference from time to time!
  static final String remoteTestInputDirectory =
    "https://bitbucket.org/mmodrow/media_downloader/raw/027a7761919ebc6e926aee430ea3038bf22e90a9/tests/testInput/";
  
  /**
   * gets the proper input dir; depending on cwd
   */
  static String getTestInputDirectory(){
    return getTestDirectory(testInputDirectory);
  }
  
  /**
   * gets the proper temp dir; depending on cwd
   */
  static String getTestTempDirectory(){
    return getTestDirectory(testTempDirectory);
  }
  
  /**
   * gets a given directory and adds prefix if needed
   */
  @SuppressWarnings("WeakerAccess")
  static String getTestDirectory(String dir){
    if((new File(dir)).exists()){
      return dir;
    } else {
      return directoryPrefix+dir;
    }
  }
  /*
      delete a specific file or folder.
      @link http://stackoverflow.com/a/3775893
       */
  @SuppressWarnings("WeakerAccess")
  static void deleteFileOrFolder (final Path path) throws IOException {
    //noinspection SameReturnValue
    Files.walkFileTree(path, new SimpleFileVisitor<Path>(){
      @Override public FileVisitResult visitFile(final Path file, final BasicFileAttributes attrs)
              throws IOException {
        Files.delete(file);
        return CONTINUE;
      }
      
      @Override public FileVisitResult visitFileFailed(final Path file, final IOException e) {
        return handleException(e);
      }
      
      @SuppressWarnings("SameReturnValue")
      private FileVisitResult handleException(final IOException e) {
        e.printStackTrace(); // replace with more robust error handling
        return TERMINATE;
      }
      
      @Override public FileVisitResult postVisitDirectory(final Path dir, final IOException e)
              throws IOException {
        if(e!=null)return handleException(e);
        Files.delete(dir);
        return CONTINUE;
      }
    });
  }
  
  /**
   * delete whatever test data had to be put into the file system
   */
  static void deleteTestData () {
    String target = getTestTempDirectory();
    Path path = Paths.get(target);
    if(new File(target).exists()) {
      try {
        deleteFileOrFolder(path);
      } catch (IOException | NullPointerException e) {
        //e.printStackTrace();
      }
    }
  }
}
