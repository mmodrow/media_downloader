package Config;

import org.w3c.dom.Node;

/**
 * Created by mmodrow on 10.07.2017.
 */
public class Nsfw extends ReadConfig {
  
  @SuppressWarnings("WeakerAccess")
  protected static final Nsfw NSFW_INSTANCE = new Nsfw();
  
  public Nsfw () {
    super();
  }
  
  static Nsfw getInstance () {
    return NSFW_INSTANCE;
  }
  
  /**
   * get the selector that's looked at to determine if a page is flagged nsfw
   */
  public static String selector (String domainName) {
    if(null == domainName || 0 == domainName.length()) { return null; }
    String xPathRequestString = "/domains/domain[@name='" + domainName + "']/nsfw/selector";
    return getInstance().getConfigPathElementString(xPathRequestString, Domain.configStatic());
  }
  
  /**
   * get the pattern that's applied to the nsfw-selector
   */
  public static Node pattern (String domainName) {
    if(null == domainName || 0 == domainName.length()) { return null; }
    String xPathRequestString = "/domains/domain[@name='" + domainName + "']/nsfw/pattern";
    return getInstance().getConfigPathFirstElement(xPathRequestString, Domain.configStatic());
  }
  
  /**
   * get the pattern that's applied to the nsfw-selector
   */
  public static String directory (String domainName) {
    if(null == domainName || 0 == domainName.length()) { return null; }
    String xPathRequestString = "/domains/domain[@name='" + domainName + "']/nsfw/directory";
    return getInstance().getConfigPathElementString(xPathRequestString, Domain.configStatic());
  }
  
  /**
   * get the DataType to find the aspect of the nsfw-element to interpret
   */
  public static Node dataType (String domainName) {
    if(null == domainName || 0 == domainName.length()) { return null; }
    String xPathRequestString = "/domains/domain[@name='" + domainName + "']/nsfw/data";
    return getInstance().getConfigPathFirstElement(xPathRequestString, Domain.configStatic());
  }
}
