package Caching;

import Notification.FlashBack;
import Text.Logger;

import java.util.concurrent.Callable;

/**
 * implements a callable action for the cache to use
 */
public abstract class CacheAction<E> implements Callable<E> {

  // constructor & action need to be implemented by child class
  abstract protected E action() throws Exception;

  public E call() {
    try {
      return action();
    } catch (Exception e){
      FlashBack.errorFlash();
      Logger.log(e.getMessage(), true, true);
      return null;
    }
  }
}