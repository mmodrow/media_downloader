package Text;

import org.junit.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;

public class StringUtilsTest extends StringUtils
{
  private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
  private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
  
  @Before
  public void setUpStreams() {
    System.setOut(new PrintStream(outContent));
    System.setErr(new PrintStream(errContent));
  }
  
  @After
  public void cleanUpStreams() {
    System.setOut(null);
    System.setErr(null);
  }
  
  @BeforeClass
  public static void oneTimeSetUp() {
    // one-time initialization code
  }
  
  @AfterClass
  public static void oneTimeTearDown() {
    // one-time cleanup code
  }
  
  @Before
  public void setUp() {
    //setup for each individual test
  }
  
  @After
  public void tearDown() {
    //de-setup for each individual test
  }
  
  /**
   * test if the escaping of umlauts as latin letters
   */
  @Test
  public void replaceUmlautsTest(){
    assertEquals( "AeaoeUEOUe ssOeaueAE", Replace.replaceUmlauts( "ÄaöÜOÜ ßÖaüÄ" ) );
  }
  
  /**
   * tests the function to properly add missing protocols for placeholder "//domainname.tld"-URLs
   */
  @Test
  public void urlPrependProtocolNoSubdomainTest(){
    assertEquals( "http://test.com", StringUtilsTest.urlPrependProtocol( "http://test.com" ) );
  }
  
  @Test
  public void urlPrependProtocolSubdomainTest(){
      assertEquals( "http://www.test.com", StringUtilsTest.urlPrependProtocol( "http://www.test.com" ) );
  }
  
  @Test
  public void urlPrependProtocolHttpsTest(){
      assertEquals( "https://www.test.com", StringUtilsTest.urlPrependProtocol( "https://www.test.com" ) );
  }
  
  @Test
  public void urlPrependProtocolnoProtocolTest(){
      assertEquals( "http://www.test.com", StringUtilsTest.urlPrependProtocol( "//www.test.com" ) );
    }
  
  /**
   * test the line counter function
   */
  @Test
  public void countLinesNoBreaksTest(){
    assertEquals(1, StringUtilsTest.countLines("asdasddsaasddsa"));
  }
  
  
  @Test
  public void countLinesBreaksTest(){
    assertEquals(4, StringUtilsTest.countLines("asd\r\nasdd\nsaas\rddsa"));
  }
  
  /**
   * stip bad chars
   */
  @Test
  public void removeIllegalCharactersTest(){
    assertEquals("asdasd", Replace.removeIllegalCharacters("as´`'\"’.‘dasd"));
  }
  
  /**
   * stip bad chars
   */
  @Test
  public void replaceBadCharactersByUnderscoreTest(){
    assertEquals( "__,-_ab_________________c_",
            Replace.replaceBadCharactersByUnderscore( "__,-_ab_â__âµ/=&%$<<>µ—_c_") );
  }
  
  /**
   * escape most English contractions to their written-out form
   */
  @Test
  public void replaceContractionsTest(){
    assertEquals("I will what you will not and He would what he has and she is not.",
            Replace.replaceContractions("I'll what you won´t and He’d what he‘s and she isn't."));
  }
  
  @Test
  public void removeNonUmlautDiacriticsTest(){
    assertEquals(Replace.removeNonUmlautDiacritics("Pokémon ís ôvèr thë tõp"), "Pokemon is over the top");
  }
  
  @Test
  public void recreateSwearingTest(){
    assertEquals(Replace.recreateSwearing("F**k those f*cking **** b%tch F*~KERS, B#tch!"), "Fuck those fucking shit bitch FUCKERS, Bitch!");
  }
  
  
  /**
   * console logging
   */
  @Test
  public void logTestStringFalseFalse() {
    Logger.log("test for empty", false, false);
    assertEquals("", outContent.toString());
  }
  
  @Test
  public void logTestStringFalseTrue() {
    Logger.log("test for empty", false, true);
    assertEquals("test for empty", outContent.toString());
  }
    
  @Test
  public void logTestStringTrueTrue() {
    Logger.log("test for empty", true, true);
    assertEquals("test for empty"+System.lineSeparator(), outContent.toString());
  }
  
  @Test
  public void logTestStringTrueTrueFollowup() {
    Logger.log("", true, true);
    assertEquals(System.lineSeparator(), outContent.toString());
  }
  
  /**
   * console logging
   */
  @Test
  public void logTestStringFalse() {
    Logger.log("test for empty", false);
    assertEquals("", outContent.toString());
  }
  
  @Test
  public void logTestStringTrue() {
    Logger.log("test for empty", true);
    assertEquals("", outContent.toString());
  }
  
  @Test
  public void logTestString() {
    Logger.log("test for empty");
    assertEquals("", outContent.toString());
  }
  
  /**
   * console logging
   */
  @Test
  public void logTestIntFalseFalse() {
    Logger.log(123, false, false);
    assertEquals("", outContent.toString());
  }
  @Test
  public void logTestIntFalseTrue() {
    Logger.log(456, false, true);
    assertEquals("456", outContent.toString());
  }
  @Test
  public void logTestIntTrueTrue() {
    Logger.log(789, true, true);
    assertEquals("789"+System.lineSeparator(), outContent.toString());
  }
  
  /**
   * console logging
   */
  @Test
  public void logTestLongTrueTrue() {
    Logger.log(456L, true, true);
    assertEquals("456"+System.lineSeparator(), outContent.toString());
  }
  @Test
  public void logTestFloatTrueTrue() {
    Logger.log(789.012F, true, true);
    assertEquals("789.012"+System.lineSeparator(), outContent.toString());
  }
  
  /**
   * console logging
   */
  @Test
  public void logTestIntFalse() {
    Logger.log(123, false);
    assertEquals("", outContent.toString());
  }
  @Test
  public void logTestIntTrue() {
    Logger.log(456, true);
    assertEquals("", outContent.toString());
  }
  @Test
  public void logTestInt() {
    Logger.log(789);
    assertEquals("", outContent.toString());
  }
  
  /**
   * console logging
   */
  @Test
  public void logTestTrueFalseFalse() {
    Logger.log(true, false, false);
    assertEquals("", outContent.toString());
  }
  @Test
  public void logTestFalseTrueTrue() {
    Logger.log(false, true, true);
    assertEquals("false"+System.lineSeparator(), outContent.toString());
  }
  @Test
  public void logTestTrueFalseTrue() {
    Logger.log(true, false, true);
    assertEquals("true", outContent.toString());
  }
  
  /**
   * console logging
   */
  @Test
  public void logTestTrueFalse() {
    Logger.log(true, false);
    assertEquals("", outContent.toString());
  }
  @Test
  public void logTestTrue() {
    Logger.log(true);
    assertEquals("", outContent.toString());
  }
}