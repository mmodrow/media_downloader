package Caching;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by mmodrow on 19.11.2016.
 * handles caching of elements of arbitrary types
 */
public class Cache <T> {
  
  // default splitter between parts of the cache map selector
  public static final String cacheDivisor = "<||>";
  private static final int    MAX_CACHE_SIZE = 20;
  private final int           instance_max_cache_size;
  // here the cached data gets saved
  private final ConcurrentHashMap<String, T> cacheMap = new ConcurrentHashMap <>();
  
  /**
    * constructor
    */
  public Cache () {
    instance_max_cache_size = 0;
  }
  /**
   * constructor
   */
  @SuppressWarnings("unused")
  public Cache (int limit) {
    instance_max_cache_size = limit;
  }
  
  /*
   * create a selector from a list of parts
   */
  public String getSelector (String[] selectorParts) {
    String output = "";
    if(null == selectorParts){
      return output;
    }
    
    for (int i = 0; i < selectorParts.length; i++) {
      output += selectorParts[i];
      if ( (i + 1) < selectorParts.length ) {
        output += cacheDivisor;
      }
    }
    
    return output;
    
  }
  
  /**
    * get a cached element from a list of selector parts
    */
  public T get (String[] selectorList){
    String selector = getSelector(selectorList);
    return get(selector);
  }
  
  /**
    * get a cached element from a selector string
    */
  public T get (String selector){
    if(null == selector || selector.equals("")) {return null; }
    return cacheMap.get(selector);
  }
  
  /**
    * use a callable object to get a value to set non-cached elements
    */
  public T get(String[] selectorList, CacheAction<T> callable){
    String selector = getSelector(selectorList);
    return get(selector, callable);
  }
  
  
  /**
    * use a callable object to get a value to set non-cached elements
    */
  public T get(String selector, CacheAction<T> callable){
    T elem = get(selector);
    
    if(null == elem){
      elem = callable.call();
      set(selector, elem);
      return elem;
    }
    return elem;
    
  }
  /*
   * try to set an element value using a selector list
   */
  public boolean set (String[] selectorList, @SuppressWarnings("SameParameterValue") T value){
    String selector = getSelector(selectorList);
    return set(selector, value);
  }
  
  /*
   * try to set an element value using a selector String
   */
  public boolean set (String selector, T value){
    if(null == selector || selector.equals("") || null == value) {return false; }
    //TODO: look for better garbage collection techniques - remove eldest elements?
    purgeIfOverfull();
    cacheMap.put(selector, value);
    return true;
  }
  
  /**
   * free up a single item from the cache
   */
  @SuppressWarnings("unused")
  public void unset(String[] selectorList){
    String selector = getSelector(selectorList);
    unset(selector);
  }
  
  @SuppressWarnings("WeakerAccess")
  public void unset(String selector){
    if(null == selector || selector.equals("")) {return; }
    cacheMap.remove(selector);
  }
  
  /**
    * prevent memory leaks by hugging too much data
    */
  @SuppressWarnings("WeakerAccess")
  protected void purgeIfOverfull(){
    int max = instance_max_cache_size > 0? instance_max_cache_size:MAX_CACHE_SIZE;
    if ( cacheMap.size() > max) {
      purge();
    }
  }
  
  /**
    * free up all memory used by this cache's cargo data
    */
  public void purge(){
    cacheMap.clear();
  }
  
}