package Config;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Created by mmodrow on 10.07.2017.
 */
public class Credentials extends ReadConfig {
  @SuppressWarnings("WeakerAccess")
  protected static final String CREDENTIALS_CFG_PATH = "credentials.xml";
  
  @SuppressWarnings("WeakerAccess")
  protected static final Credentials CREDENTIALS_INSTANCE = new Credentials();
  
  protected Credentials () {
    super();
  }
  
  static Credentials getInstance () {
    return CREDENTIALS_INSTANCE;
  }
  
  /**
   * gets the credentials to log in to a specific web site
   * @return String Array containing 0:Username and 1:Password
   */
  public static String forDomain (String domainName) {
    if(null == domainName || 0 == domainName.length()) { return null; }
    // get domain node from credentials-file
    NodeList nodeListUsername = getInstance().getConfigPathElements("/domains/domain[@name='" + domainName + "']", configStatic());
    String credentials = "";
    // iterate over all nodes and stitch them together
    Node elem = null != nodeListUsername?nodeListUsername.item(0).getFirstChild():null;
    while(null != elem && null != elem.getNextSibling()){
      // drop irrelevant nodes & pseudo-nodes
      if(!(null == elem.getNodeName() || "#text".equals(elem.getNodeName()))){
        String value = elem.getNodeValue();
        if(null == value && null != elem.getFirstChild() && null != elem.getFirstChild().getNodeValue()){
          value = elem.getFirstChild().getNodeValue();
        }
        credentials += elem.getNodeName() + "=" + value + "&";
      }
      elem = elem.getNextSibling();
    }
    // if 0 nodes are present avoid array index out of bonds exception
    credentials = credentials.substring(0,Math.max(credentials.length()-1,0));
    return credentials;
  }
  
  public static org.w3c.dom.Document configStatic (){
    return getInstance().config();
  }
  
  /**
   * reads the configuration for login credentials from the credentials xml
   *
   * @return XML-Document from the config file
   */
  public org.w3c.dom.Document config () {
    return getConfig(CREDENTIALS_CFG_PATH);
  }
}
