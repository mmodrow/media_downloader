import IO.FileSystem;
import org.junit.*;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

import static org.junit.Assert.*;

public class FileSystemTest extends FileSystem
{
  private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
  private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
	
	private final String fileNamePart1 = "3xC8411fNI6ERcnkC2C9RfW8M2kAssTAGFGhzFO30mEaTXtMIauJuYoozEHZ98fWAQvElApUQQVrkNjCnPwUAlIPE6N89wC0sEjk6N0MpzRJAWUvMshnJCe3GJUWyQLQ1EZrgzKxCWxV";
  private final String fileNamePart2 = "3xC8411fNI6ERcnkC2C9RfW8M2kAssTAGFGhzFO30mEaTXtMIauJuYoozEHZ98fWAQvElApUQQVrkNjCnPwUAlI";
  private final String fileNamePart3 = "PE6N89wC0sEj";
  private final String fileNamePart4 = "k6N0";
  private final String fileNamePart5 = "MpzRJAWUvMshnJCe3GJUWyQLQ1EZrgzKxCWxV";
  
  private static long timestamp;
  private static String tempDir;
  private static String inputDir;
  private static String timeDir;
  
  
  @BeforeClass
  public static void oneTimeSetUp() {
    // one-time initialization code
  }
  
  @AfterClass
  public static void oneTimeTearDown() {
    TestUtils.deleteTestData();
    timestamp = 0;
  }
  
  @Before
  public void setUp() {
    //setup for each individual test
    setUpStreams();
    cleanupFiles();
    
    timestamp = System.currentTimeMillis();
    tempDir = TestUtils.getTestTempDirectory();
    timeDir = tempDir+timestamp+File.separator;
    inputDir = TestUtils.getTestInputDirectory();
    createDirIfNeeded(timeDir);
  }
  
  private void setUpStreams () {
    System.setOut(new PrintStream(outContent));
    System.setErr(new PrintStream(errContent));
  }
  
  private void cleanupFiles (){
    TestUtils.deleteTestData();
  }
  
  
  @After
  public void tearDown() {
    //de-setup for each individual test
    cleanUpStreams();
  }
  
  private void cleanUpStreams () {
    System.setOut(null);
    System.setErr(null);
  }
  /**
   * limit file name length
   */
  @Test
  public void limitFileNameLengthKeepTest() {
    assertEquals(fileNamePart1, limitFileNameLength(fileNamePart1,0));
  }
    
  @Test
  public void limitFileNameLengthShortTest() {
    String input           = fileNamePart1+fileNamePart2+fileNamePart3+fileNamePart4+fileNamePart5;
    String expectedOutput  = fileNamePart1+fileNamePart2+fileNamePart3;
    assertEquals(expectedOutput, limitFileNameLength(input,0));
  }
    
  @Test
  public void limitFileNameLengthShortenedTest() {
    String input           = fileNamePart1+fileNamePart2+fileNamePart3+fileNamePart4+fileNamePart5;
    String expectedOutput  = fileNamePart1+fileNamePart2;
    assertEquals(expectedOutput, limitFileNameLength(input,12));
  }
    
  @Test
  public void limitFileNameLengthShortenedNegativeTest() {
    String input           = fileNamePart1+fileNamePart2+fileNamePart3+fileNamePart4+fileNamePart5;
    String expectedOutput  = fileNamePart1+fileNamePart2+fileNamePart3+fileNamePart4;
    assertEquals(expectedOutput, limitFileNameLength(input,-4));
  }
  
  @Test(expected=NullPointerException.class)
  public void limitFileNameLengthNullTest() {
    limitFileNameLength(null, 0);
  }
  
  
  /**
   * create needed dirs and return false if something was wrong
   */
  @Test
  public void createDirIfNeededRelativeCreationTest() {
    createDirIfNeeded(timeDir+"created"+File.separator);
    File createdDir = new File(timeDir+"created"+File.separator);
    assertTrue(createdDir.exists());
  }
  /*@Test
  public void createDirIfNeededAbsoluteCreationTest() {
    File createdDir = new File((timeDir+"created").replaceAll("^\\.*[/\\\\]", ""));
    String absPath = createdDir.getAbsolutePath();
    createDirIfNeeded(absPath);
    createdDir = new File(absPath);
    assertTrue(createdDir.exists());
  }*/
  @Test
  public void createDirIfNeededNoCollisionTest() {
    assertTrue(FileSystem.createDirIfNeeded(timeDir));
  }
  @Test
  public void createDirIfNeededCollisionTest() {
    assertTrue(createDirIfNeeded(timeDir));
    assertTrue(createDirIfNeeded(timeDir));
  }
  
  @Test
  public void createDirIfNeededIllegalCharTest() {
    assertFalse(createDirIfNeeded(tempDir+"'< > : \" / \\ | ? *"+((char) 0)));
  }
    
  @Test
  public void createDirIfNeededEmptyTest() {
    assertFalse(createDirIfNeeded(""));
  }
  
  @Test
  public void createDirIfNeededNullTest() {
    assertFalse(createDirIfNeeded(null));
  }
  
  @Test
  public void dlPathNoConflictNewTest(){
    assertEquals(timeDir+"testFile.txt", dlPathNoConflict(timeDir+"testFile.txt"));
  }
  
  @Test
  public void dlPathNoConflictExistingTest() throws IOException {
    String fileName = timeDir+"testFile.txt";
    File newFile = new File(fileName);
    boolean fileCreated = newFile.createNewFile();
    assertTrue(fileCreated);
    assertNotEquals(fileName, dlPathNoConflict(fileName));
  }
  
  /**
   * test the conflict-evasion for file names
   */
  @Test
  public void dlPathNoConflictEmptyTest() {
    assertNotEquals("", dlPathNoConflict(""));
  }
  
  @Test
  public void dlPathNoConflictFileEndingOnlyTest() {
    String path = timeDir+File.separator+".txt";
    assertNotEquals(path, dlPathNoConflict(path));
  }
  
  @Test
  public void dlPathNoConflictNullTest() {
    assertEquals("", dlPathNoConflict(null));
  }
    
  @Test
  public void dlPathNoConflictFileTest() {
    String path = timeDir+File.separator+"test.txt";
    assertEquals(path, dlPathNoConflict(path));
  }
  
  @Test
  public void dlPathNoConflictFileExistsTest() {
    String path = timeDir+File.separator+"test.txt";
    // Use relative path for Unix systems
    File f = new File(path);
    boolean fileCreated = false;
    try {
      fileCreated = f.createNewFile();
    } catch (IOException e) {
      e.printStackTrace();
    }
    // just in casre something went wrong with the file creation, to prevent false positives
    assertTrue(fileCreated);
    assertNotEquals(path, dlPathNoConflict(path));
  }
  
  @Test
  public void getFileSizeEmptyTest() {
    assertEquals(0, getFileSize(inputDir+"empty.txt"));
  }
  
  @Test
  public void getFileSizeNotEmptyTest() {
    assertEquals(59948, getFileSize(inputDir+"notEmpty.txt"));
  }
  
  @Test(expected=NullPointerException.class)
  public void getFileSizeNullTest() {
    getFileSize(null);
  }
  
  
  @Test
  public void isImageOKTest() {
    assertTrue(isImage(inputDir+"white_image.png"));
  }
  
  @Test
  public void isImageTextTest() {
    assertFalse(isImage(inputDir+"notEmpty.txt"));
  }
  
  @Test
  public void isImageTextAsJpgTest() {
    assertFalse(isImage(inputDir+"notAnImage.jpg"));
  }
  
  @Test
  public void isImageNotExistentTest () {
    assertFalse(isImage(inputDir+"veryVeryEmpty.txt"));
  }
  
  @Test
  public void isImageNullTest() {
    assertFalse(isImage(null));
  }
  
}