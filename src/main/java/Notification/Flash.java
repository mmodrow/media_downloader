package Notification;

import java.awt.*;
import java.util.Timer;
import java.util.TimerTask;

/**
 * A task designed to flash to a certain colour and then trigger a flash back to white
 */
@SuppressWarnings("WeakerAccess")
public class Flash extends TimerTask {

  public static Component defaultTarget = null;
  
  protected final Component target;
  @SuppressWarnings("WeakerAccess")
  protected final Color c;
  @SuppressWarnings("WeakerAccess")
  protected final long timeout;
  @SuppressWarnings("WeakerAccess")
  protected final int numFlashes;

  /**
   * Constructor
   * @param target The Component to flash
   * @param timeout timeout in ms
   * @param numFlashes amount of flashes to c and back to white
   */
  public Flash (Component target, Color c, long timeout, int numFlashes){
    this.target = target;
    this.c = c;
    this.timeout = timeout;
    this.numFlashes = numFlashes;
  }
  
  /**
   * Flash to a certain color and back a couple of times.
   * @param target the target component to flash
   * @param timeout timeout in ms
   * @param numFlashes amount of flashes to c and back to white
   */
  @SuppressWarnings("WeakerAccess")
  public static synchronized void init (Component target, Color c, @SuppressWarnings("SameParameterValue") long timeout, @SuppressWarnings("SameParameterValue") int numFlashes){
    Timer timer = new Timer();
    timer.schedule( new Flash(target, c, timeout, numFlashes), 0);
  }

  /**
   * do the flashing
   */
  @Override public void run()
  {
    // stop flashing when done
    if (0 == numFlashes) {
      target.setBackground(Color.WHITE);
      return;
    }
    target.setBackground(c);
    // switch back to white
    Timer timer = new Timer();
    timer.schedule( new FlashBack(target, c, timeout, numFlashes), timeout);
  }
}
