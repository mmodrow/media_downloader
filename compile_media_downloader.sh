#! /bin/bash
mvn --version>/dev/null 2>&1 || { 
  echo "I require maven3 but it's not installed." >&2; 
  AVAILABLE="$(apt-cache policy maven | grep " 3." | head -1 | sed 's/.*: //g')"
  echo "Maven version ${AVAILABLE} would be available. Install it by executing:"
  echo "   sudo apt-get install maven=${AVAILABLE}"
}
mvn compile
mvn clean compile assembly:single
mvn test