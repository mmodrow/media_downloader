package Config;

/**
 * Created by mmodrow on 10.07.2017.
 */
public class Login extends Credentials
{
  
  
  @SuppressWarnings("WeakerAccess")
  protected static final Login LOGIN_INSTANCE = new Login();
  
  public Login () {
    super();
  }
  
  static Login getInstance () {
    return LOGIN_INSTANCE;
  }
  
  /**
   * get the selector that's present when login is required
   */
  public static String loginRequiredSelector (String domainName) {
    if(null == domainName || 0 == domainName.length()) { return null; }
    String xPathRequestString = "/domains/domain[@name='" + domainName + "']/loginRequired/selector";
    return getInstance().getConfigPathElementString(xPathRequestString, Domain.configStatic());
  }
  
  /**
   * get the selector that's present when login is required
   */
  public static String url (String domainName) {
    if(null == domainName || 0 == domainName.length()) { return null; }
    String xPathRequestString = "/domains/domain[@name='" + domainName + "']/loginRequired/loginUrl";
    return getInstance().getConfigPathElementString(xPathRequestString, Domain.configStatic());
  }
}
