package IO;


import Config.Credentials;
import Config.Login;
import Notification.FlashBack;
import Text.Logger;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.net.*;
import java.nio.file.Paths;
import java.util.ArrayList;

//import sun.misc.IOUtils;

/**
 * Created by mmodrow on 14.11.2016.
 * Handles all remote access functionality that is targeted at mere requests without interpretation of the response body
 */
public abstract class HttpRequest {
  
  // --Commented out by Inspection (12.12.2016 20:06):protected static final String ESCAPED_FILE_SEPARATOR = "/".equals(File.separator) ? "/" : "\\";
  @SuppressWarnings("WeakerAccess")
  public static final String DEFAULT_USER_AGENT = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11";
  // semi-permanent settings
  @SuppressWarnings("WeakerAccess")
  public static final int DEFAULT_TIMEOUT = 5000;
  
  /**
   * TODO: Comment.
   * @link https://www.mkyong.com/java/how-to-automate-login-a-website-java-example/
   */
  @SuppressWarnings("WeakerAccess")
  public static int sendPost (String url, String postParams) throws Exception {
    HttpURLConnection conn = preparePostConnection(url, postParams);
    
    // Send post request
    DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
    wr.writeBytes(postParams);
    wr.flush();
    wr.close();
    
    int responseCode = conn.getResponseCode();
    Logger.log("\nSending 'POST' request to URL : " + url, true);
    Logger.log("Post parameters : " + postParams, true);
    Logger.log("Response Code : " + responseCode, true);
    
    BufferedReader in =
            new BufferedReader(new InputStreamReader(conn.getInputStream()));
    String inputLine;
    StringBuilder response = new StringBuilder();
    
    while ((inputLine = in.readLine()) != null) {
      response.append(inputLine);
    }
    
    in.close();
    Logger.log(response.toString(), true);
    return responseCode;
  }
  
  public static String sendGet(String url){
    try {
      return sendGet(new URL(url));
    } catch (MalformedURLException e) {
      e.printStackTrace();
    }
    return "";
  }
  
  public static String sendGet(URL url) {
    URLConnection con;
    String body;
    try {
      con = url.openConnection();
      InputStream in = con.getInputStream();
      String encoding = con.getContentEncoding();
      encoding = encoding == null ? "UTF-8" : encoding;
      body = IOUtils.toString(in, encoding);
    } catch (IOException e) {
      e.printStackTrace();
      body = "";
    }
    return body;
  }
  
  
  /**
   * TODO: Comment.
   */
  private static HttpURLConnection preparePostConnection (String url, String postParams) throws IOException {
    URL obj = new URL(url);
    HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
    
    // Acts like a browser
    conn.setUseCaches(false);
    conn.setRequestMethod("POST");
    conn.setRequestProperty("Host", url.replaceAll("^https?://([^/]*)(?:/.*)?$", "$1"));
    conn.setRequestProperty("User-Agent", DEFAULT_USER_AGENT);
    conn.setRequestProperty("Accept",
            "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
    conn.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
    conn.setConnectTimeout(DEFAULT_TIMEOUT);
    
    if ( null == CookieHandler.getDefault()){
      // make sure cookies is turn on
      CookieHandler.setDefault(new CookieManager());
    }
    
    conn.setRequestProperty("Connection", "keep-alive");
    conn.setRequestProperty("Referer", url);
    conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
    conn.setRequestProperty("Content-Length", Integer.toString(postParams.length()));
    
    conn.setDoOutput(true);
    conn.setDoInput(true);
    return conn;
  }
  
  /**
   * try to perform a login
   */
  public static boolean loginToDomain (String domainName){
    try {
      Logger.log("Trying to log in to "+domainName, true, true);
      String credentials = Credentials.forDomain(domainName);
      String loginUrl = Login.url(domainName);
      if(null == loginUrl || loginUrl.equals("")){
        FlashBack.errorFlash();
        Logger.log("No login url was found for "+domainName, true, true);
        return false;
      }
      
      if(null == credentials ||credentials.length() < 2){
        FlashBack.errorFlash();
        Logger.log("No credentials were found for "+domainName, true, true);
        return false;
      }
      
      int responseCode = sendPost(loginUrl, credentials);
      Logger.log("Response code: "+responseCode, true, true);
      return true;
    } catch (Exception e) {
      e.printStackTrace();
      return false;
    }
  }
  
  /**
   * tests if the response code for a given url is 200 or a redirect
   */
  public static boolean isUrlReachable(String targetUrl) {
    if(null == targetUrl ||targetUrl.equals("")){return false;}
    
    try {
      URL url = new URL(targetUrl.trim().replace("%0A", ""));
      HttpURLConnection conn = (HttpURLConnection) url.openConnection();
  
      // Acts like a browser
      conn.setUseCaches(false);
      conn.setRequestMethod("GET");
      conn.setRequestProperty("Host", targetUrl.replaceAll("^https?://([^/]*)(?:/.*)?$", "$1"));
      conn.setRequestProperty("User-Agent", DEFAULT_USER_AGENT);
      conn.setConnectTimeout(DEFAULT_TIMEOUT);
      // HttpURLConnection.setFollowRedirects(false);
      // note : you may also need
      // HttpURLConnection.setInstanceFollowRedirects(false)
      int responseCode = conn.getResponseCode();
      return (responseCode == HttpURLConnection.HTTP_OK
        || (responseCode >= HttpURLConnection.HTTP_MULT_CHOICE
          && responseCode < HttpURLConnection.HTTP_BAD_REQUEST
        )
      );
    } catch (Exception e) {
      FlashBack.errorFlash();
      e.printStackTrace();
      return false;
    }
  }
  
  /**
   * get a file and put it to a specified place
   */
  public static String download(String url, String localFile) throws Exception {
    String cwd = Paths.get(".").toAbsolutePath().normalize().toString().replace("\\", "\\\\");
    String localFileAbsolute = localFile.replaceAll("^\\.+", cwd);
    FileSystem.createDirIfNeeded(localFileAbsolute);
    String deconflictedLocalPath = FileSystem.dlPathNoConflict(localFileAbsolute);
    //boolean downloadComplete = false;

    Logger.log("Downloading " + deconflictedLocalPath, true);
    //while (!downloadComplete) {
    boolean downloadComplete = transferData(url, deconflictedLocalPath);
    //}
    return downloadComplete?deconflictedLocalPath:null;
  }
  
  /**
   * gets parts of the given file
   *
   * @param url the remote url to be gotten
   * @param fileName the local filename
   */
  @SuppressWarnings("WeakerAccess")
  static protected boolean transferData (String url, String fileName) throws Exception {
    boolean success;
    FileOutputStream fileOutputStream = null;
    try {
      URLConnection connection = prepareDownloadConnection(url);
      byte[] response = streamBytesFromConnection(connection);
      
      fileOutputStream = new FileOutputStream(fileName);
      fileOutputStream.write(response);
  
      //noinspection UnusedAssignment
      connection = null;
      //noinspection UnusedAssignment
      response = null;
  
      // only counts as success if the downloaded medium is valid.
      validateImageOrVideo(fileName);
      success = true;
              Logger.log("\n", false, true);
    } catch (Exception e) {
      success = false;
    }
    
    if (null != fileOutputStream) {
      fileOutputStream.close();
      //noinspection UnusedAssignment
      fileOutputStream = null;
    }
    
    return success;
  }
  
  /**
   * Checks if the given filename corresponds to a valid image or video file.
   * @param fileName file name and path - if relative it's relative to the jar.
   * @return Whether the referenced file is valid.
   */
  private static boolean validateImageOrVideo (String fileName) {
    boolean fileIsImage = FileSystem.isImage(fileName);
    ArrayList<String> uncheckedFormats = new ArrayList <>();
    uncheckedFormats.add(".mp4");
    uncheckedFormats.add(".avi");
    if(!fileIsImage && !uncheckedFormats.contains(fileName.substring(fileName.length()-4))){
      Logger.log("", true, true);
      Logger.log("Downloaded file is not an image - it's either an unknown format or faulty. Please check.", true, true);
      FlashBack.errorFlash();
      return false;
    }
    
    return true;
  }
  
  /**
   * Get the content from a URLConnection's target as byte[].
   * @param connection The connection to stream.
   * @return The binary response.
   * @throws IOException The given url might be unreachable.
   */
  private static byte[] streamBytesFromConnection (URLConnection connection) throws IOException {
    //Code to download
    int logStepDistance = 10;
    int logLineBreakDistance = 150*logStepDistance;
    InputStream inputStream = connection.getInputStream();
    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
    
    byte[] byteBuffer = new byte[1024];
    int n;
    for (int i = 1;-1 != (n = inputStream.read(byteBuffer)); i++) {
      byteArrayOutputStream.write(byteBuffer, 0, n);
      Logger.log(".", i%logLineBreakDistance==0, i%logStepDistance==0);
    }
    
    byte[] response = byteArrayOutputStream.toByteArray();
    
    // close and destroy the streams and buffers.
    
    //noinspection UnusedAssignment
    byteBuffer = null;
    
    inputStream.close();
    //noinspection UnusedAssignment
    inputStream = null;
    
    byteArrayOutputStream.close();
    //noinspection UnusedAssignment
    byteArrayOutputStream = null;
    
    return response;
  }
  
  /**
   * builds up an http connection from an url
   * @param url URL to connect to.
   * @return The URLConnection to reach the given url
   * @throws IOException The given url might be unreachable.
   */
  private static URLConnection prepareDownloadConnection (String url) throws IOException {
    //Text.StringUtils.log("downloaded size: " + transferredSize);
    URL website = new URL(url);
    URLConnection connection = website.openConnection();
    connection.setRequestProperty("Host", url.replaceAll("^https?://([^/]*)(?:/.*)?$", "$1"));
    connection.setRequestProperty("User-Agent", DEFAULT_USER_AGENT);
    //connection.setRequestProperty("Range", "bytes=" + transferredSize + "-");
    connection.setConnectTimeout(DEFAULT_TIMEOUT);
    return connection;
  }
}