package Text;

/**
 * Prints logging messages into the system out.
 * Created by mmodrow on 09.07.2017.
 */
public class Logger {
  // individual settings
  private static final boolean DEBUG = false;
  
  /**
   * prints a message if debug mode is on
   *
   * @param message what needs to be printed
   */
  public static void log (String message, boolean newLine, boolean force) {
    if (DEBUG || force) {
      if (newLine) {
        System.out.println(message);
      } else {
        System.out.print(message);
      }
    }
  }
  
  /**
   * prints a message if debug mode is on
   *
   * @param message what needs to be printed
   */
  public static void log (String message, boolean newLine) {
    log(String.valueOf(message), newLine, false);
  }
  
  /**
   * prints a message if debug mode is on
   *
   * @param message what needs to be printed
   */
  public static void log (String message) {
    log(String.valueOf(message), true, false);
  }
  
  /**
   * prints a message if debug mode is on
   *
   * @param message what needs to be printed
   */
  public static void log (Number message, boolean newLine, boolean force) {
    log(String.valueOf(message), newLine, force);
  }
  
  /**
   * prints a message if debug mode is on
   *
   * @param message what needs to be printed
   */
  public static void log (Number message, boolean newLine) {
    log(String.valueOf(message), newLine, false);
  }
  
  /**
   * prints a message if debug mode is on
   *
   * @param message what needs to be printed
   */
  public static void log (Number message) {
    log(String.valueOf(message), true, false);
  }
  
  /**
   * prints a message if debug mode is on
   *
   * @param message what needs to be printed
   */
  public static void log (boolean message, boolean newLine, boolean force) {
    log(String.valueOf(message), newLine, force);
  }
  
  /**
   * prints a message if debug mode is on
   *
   * @param message what needs to be printed
   */
  public static void log (@SuppressWarnings("SameParameterValue") boolean message, @SuppressWarnings("SameParameterValue") boolean newLine) {
    log(String.valueOf(message), newLine, false);
  }
  
  /**
   * prints a message if debug mode is on
   *
   * @param message what needs to be printed
   */
  public static void log (@SuppressWarnings("SameParameterValue") boolean message) {
    log(String.valueOf(message), true, false);
  }
}